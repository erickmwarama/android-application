package com.example.marchthirty;

import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.text.DateFormat;
import java.util.*;
import android.content.*;
import android.view.*;

public class UserOrdersListViewAdapter extends  BaseAdapter
{
	private List<Order> orders;
	private Context context;
	private AccessDatabaseOrder accessorder;
	private AccessDatabaseProducts accessproducts;
	public int total=0;
	
	public UserOrdersListViewAdapter(Context con,int userid)
	{
		this.context=con;
		accessorder = new AccessDatabaseOrder(context);
		accessproducts = new AccessDatabaseProducts(context);
		accessproducts.open();
		accessorder.open();
		orders = accessorder.getUserOrders(userid);
	}
	
	public String getDateString(Long time)
	{
		Date date = new Date(time);
		DateFormat format = DateFormat.getDateInstance(DateFormat.MEDIUM);
		return format.format(date);
	}
	
	public int getTotalCost()
	{
		int total = 0;
		int cost = 0;
		for(int i=0;i<getCount();i++)
		{
			Order order =(Order)getItem(i);
			cost = order.getQuantity() * accessproducts.getProduct(order.getProductId()).getPrice();
			total = total +cost;
		}
		return total;
	}

	@Override
	public int getCount() {
		return orders.size();
	}

	@Override
	public Object getItem(int pos) {
		return orders.get(pos);
	}

	@Override
	public long getItemId(int arg0) {
		return orders.get(arg0).getOrderId();
	}

	@Override
	public View getView(int pos, View view, ViewGroup arg2) {
		
		Order order = (Order)getItem(pos);
		Product theproduct =accessproducts.getProduct(order.getProductId());
		int totalcost = order.getQuantity() * theproduct.getPrice();
		total = total +=totalcost;
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		View row = view;
		if(row == null)
		{
			row = inflater.inflate(R.layout.userordersfragment_listitems,arg2,false);
		}
		
		TextView count = (TextView)row.findViewById(R.id.count);
		TextView orderid = (TextView)row.findViewById(R.id.orderid);
		TextView product = (TextView)row.findViewById(R.id.userorders_productname);
		TextView price = (TextView)row.findViewById(R.id.userorders_price);
		TextView qty = (TextView)row.findViewById(R.id.userorders_qty);
		TextView date = (TextView)row.findViewById(R.id.userorders_date);
		TextView cost = (TextView)row.findViewById(R.id.userorders_total);
		
		count.setText((pos+1)+"");
		orderid.setText(order.getOrderId()+"");
		product.setText(theproduct.getProductName());
		price.setText(theproduct.getPrice()+"");
		qty.setText(order.getQuantity()+"");
		date.setText(getDateString(order.getDate()));
		cost.setText(totalcost+"");
		
		return row;
	}
	
}