package com.example.marchthirty;

import android.content.*;
import android.preference.PreferenceManager;
import android.util.Log;

public class MyPreferences
{
	private SharedPreferences prefs;
	private SharedPreferences.Editor editor;
	private Context con;
	
	public MyPreferences(Context c)
	{
		this.prefs=PreferenceManager.getDefaultSharedPreferences(c);
		this.editor=prefs.edit();
		this.con=c;
	}
	
	public void storeUser(User u)
	{
		editor.putString("name", u.getFullname());
		editor.putString("email", u.getEmail());
		editor.putString("pass", u.getPassword());
		editor.putInt("userid", u.getId());
		editor.commit();
		Log.i("MARCH THIRTY","preferences created");
	}
	
	public User retreiveUser()
	{
		User us=new User();
		us.setEmail(prefs.getString("email", "no email"));
		us.setFullname(prefs.getString("name", "no name"));
		us.setPassword(prefs.getString("pass", ""));
		us.setId(prefs.getInt("userid", -1));
		return us;
	}
	
}