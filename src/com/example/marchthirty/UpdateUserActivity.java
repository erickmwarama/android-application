package com.example.marchthirty;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.util.Log;
import android.view.View;
import  android.widget.*;
import android.content.*;

public class UpdateUserActivity extends Activity
{
	private EditText name_txt,email_txt,password_txt;
	private TextView title;
	private Button update;
	private AccessDatabaseUser accessuser;
	private User theuser;
	private Activity con;
	private MyPreferences mypref;
	public Handler han;
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.update_user_activity);
		con=this;
		Intent intent=getIntent();
		
		name_txt=(EditText)findViewById(R.id.update_name_text);
		email_txt=(EditText)findViewById(R.id.update_email_text);
		password_txt=(EditText)findViewById(R.id.update_pass_text);
		title=(TextView)findViewById(R.id.update_title);
		update=(Button)findViewById(R.id.update_btn_update);
		
		accessuser=new AccessDatabaseUser(UpdateUserActivity.this);
		mypref = new MyPreferences(UpdateUserActivity.this);
		accessuser.open();
		theuser=accessuser.getUser(intent.getIntExtra("userid",-1));
		
		name_txt.setText(theuser.getFullname());
		email_txt.setText(theuser.getEmail());
		password_txt.setText(theuser.getPassword());
		title.setText("update details for user number: "+theuser.getId());
		
		
		
		update.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				String name,email,password;
				name=name_txt.getText().toString();
				email=email_txt.getText().toString();
				password=password_txt.getText().toString();
				
				theuser.setEmail(email);
				theuser.setFullname(name);
				theuser.setPassword(password);
				
				if(checkConnection())
				{
					new UpdateUser().execute(theuser);
				}
				else
				{
					Toast.makeText(con, "sorry. You do not have internet connection", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }
	
	private static String parseJson(String string)
	{
		String str="";
		JSONObject object = null;
		try
		{
			object=new JSONObject(string);
			str=object.getString("response");
		}
		catch(JSONException exc)
		{
			
		}
		return str;
	}
	
	private static String convertStreamToString(InputStream stream) throws Exception
	{
		StringBuffer buffer=new StringBuffer();
		String line;
		BufferedReader reader=new BufferedReader(new InputStreamReader(stream));
		while((line=reader.readLine()) != null)
		{
			buffer.append(line);
		}
		return buffer.toString();
	}
	
	private static String sendToServer(String string) throws Exception
	{
		InputStream stream;
		HttpClient client=new DefaultHttpClient();
		HttpPost post = new HttpPost(NetworkAddress.updateuser);
		StringEntity entity=new StringEntity(string);
		post.setEntity(entity);
		
		HttpResponse response=client.execute(post);
		stream=response.getEntity().getContent();
		return convertStreamToString(stream);
	}
	
	private String createUserString(User user)
	{
		JSONObject json=new JSONObject();
		try
		{
			//json.put("object","update");
			json.put("userid", user.getId());
			json.put("fullname", user.getFullname());
			json.put("email", user.getEmail());
			json.put("password", user.getPassword());
		}
		catch(JSONException exc)
		{
			
		}
		return json.toString();
	}
	
	private class UpdateUser extends AsyncTask<User,Void,String>
	{
		private String data="",result="",response="";
		private User user;
		private ProgressDialog dialog;
		
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(con, "updating", "please wait...");
		}
		
		public String doInBackground(User... users)
		{
			data=createUserString(users[0]);
			
			
				if(accessuser.updateUser(users[0]))
				{
					user = mypref.retreiveUser();
					if(user.getId() == users[0].getId())
					{
						mypref.storeUser(users[0]);
					}
					try
					{
						result=sendToServer(data);
					}
					catch(Exception exc)
					{
						Log.i("march thirty",exc.getMessage());
					}
				
					response= parseJson(result);
					
					
					con.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(con, response, Toast.LENGTH_SHORT).show();
						}
					});
					
					Intent i=new Intent(UpdateUserActivity.this,ListUsersActivity.class);
					startActivity(i);
				}
				else
				{
					con.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(con, "UNABLE TO UPDATE USER", Toast.LENGTH_SHORT).show();
						}
					});
				}
			
			
			return null;
		}
		
		public void onPostExecute(String string)
		{
			
			dialog.dismiss();
			
		}
	}
}