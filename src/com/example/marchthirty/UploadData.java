package com.example.marchthirty;

import android.app.*;
import android.content.*;
import java.util.*;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;
import android.os.*;
import android.widget.*;
import android.view.*;
//import android.widget.*;
import java.io.*;

public class UploadData
{
	private Context con;
	//private String uploaddata;
	private AccessDatabaseUser accessuser;
	private AccessDatabaseCart accesscart;
	private AccessDatabaseOrder accessorder;
	private AccessDatabaseProducts accessproducts;
	public ProgressDialog dialog;
	private int resultid;
	private MyPreferences prefs;
	private List<Order> orders;
	private Boolean hasViews,hasIntent,todelete;
	private Intent i;
	private List<View> viewlist;
	public static String disptitle;
	public static String server="http://192.168.56.1:80/myapp/appServer.php";;
	
	public UploadData(Context c,User user,List<View> views,Intent intent,boolean delete)
	{
		this.con=c;
		if(views != null)
		{
			this.hasViews=true;
		}
		else
		{
			this.hasViews=false;
		}
		this.todelete=delete;
		this.hasIntent=true;
		this.i=intent;
		this.viewlist=views;
		if(delete)
		{
			disptitle="deleting";
		}
		else
		{
			disptitle="uploading";
		}
		
		accessuser = new AccessDatabaseUser(con);
		prefs=new MyPreferences(con);
		new UploadingDataToServer().execute(user,"user");
		
	}
	
	public UploadData(Context c,MyCart cart,Intent intent)
	{
		this.con=c;
		this.hasViews=false;
		this.hasIntent=true;
		this.i=intent;
		disptitle="uploading";
		accesscart = new AccessDatabaseCart(con);
		new UploadingDataToServer().execute(cart,"cart");
		//dialog=ProgressDialog.show(con,"uploading","please wait...",true);
	}
	
	public UploadData(Context c)
	{
		this.con=c;
		this.hasViews=false;
		this.hasIntent=false;
		disptitle="downloading";
		accessproducts = new AccessDatabaseProducts(con);
		new UploadingDataToServer().execute("","products");
		//dialog=ProgressDialog.show(con,"downloading","please wait...",true);
	}
	
	private String createUserString(User u)
	{
		JSONObject json=new JSONObject();
		try{
		json.put("object", "user");
		if(todelete)
		{
			json.put("delete", true);
		}
		json.accumulate("userid",accessuser.getUserId(u.getFullname(),u.getPassword()));
		json.accumulate("fullname", u.getFullname());
		json.accumulate("email",u.getEmail());
		json.accumulate("password", u.getPassword());
		}catch(JSONException exc)
		{
			
		}
		return json.toString();
	}
	
	private String createCartString(MyCart cart)
	{
		orders=cart.getOrders();		
		
		JSONObject json=new JSONObject();
		for(int i=0;i<orders.size();i++)
		{
			Order order=orders.get(i);
			try
			{
				//json.put("object", "cart");
				json.put("userid",order.getUserid());
				json.put("cartid", order.getCartId());
				json.accumulate("orderid",order.getOrderId() );
				json.accumulate("productid",order.getProductId());
				json.accumulate("quantity", order.getQuantity());
				json.accumulate("date", order.getDate());
			}
			catch(JSONException exc)
			{
				
			}
		}
		
		return json.toString();
	}
	
	private String createProductString()
	{
		JSONObject json=new JSONObject();
		try
		{
			json.put("object", "products");
		}
		catch(JSONException exc)
		{
			
		}
		
		return json.toString();
	}
	
	/*private String createOrderString(Order o)
	{
		JSONObject jb=new JSONObject();
		try
		{
			jb.accumulate("orderid",adb.getOrderId(o));
			jb.accumulate("productid", o.getProductId());
			jb.accumulate("quantity",o.getQuantity());
			jb.accumulate("userid",o.getUserid());
			jb.accumulate("date", o.getDate());
			
		}
		catch(JSONException exc)
		{
			
		}
		return jb.toString();
	}
	*/
	private static String convertStreamToString(InputStream is) throws IOException
	{
		String line;
		BufferedReader reader=new BufferedReader(new InputStreamReader(is));
		StringBuffer buffer=new StringBuffer();
		while((line=reader.readLine())!=null)
		{
			buffer.append(line);
		}
		return buffer.toString();
	}
	
	private  void extractProducts(String[] strings)
	{
		//String[] strings=s.split(":");
		String[]productnames=strings[0].split(",");
		String[] prices=strings[1].split(",");
		String[] weights=strings[2].split(",");
		for(int i=0;i<productnames.length;i++)
		{
			Product pro=new Product();
			pro.setProductName(productnames[i]);
			pro.setPrice(Integer.valueOf(prices[i]));
			pro.setWeight(weights[i]);
			accessproducts.insertProduct(pro);
		}
	}
	
	private  void parseJsonObject(JSONObject jsonobject)
	{
		if(jsonobject == null)
		{
			
		}
		else
		{
			try
			{
				String type=jsonobject.getString("return");
				if(type.equals("user"))
				{
					Toast.makeText(con, jsonobject.getString("response"), Toast.LENGTH_SHORT).show();
				}
				else
				{
					if(type.equals("cart"))
					{
						Toast.makeText(con, jsonobject.getString("response"), Toast.LENGTH_SHORT).show();
					}
					else
					{
						if(type.equals("products"))
						{
							String[] s=new String[]{jsonobject.getString("names"),jsonobject.getString("prices"),jsonobject.getString("weights")};
							extractProducts(s);
							Toast.makeText(con, "OKAY", Toast.LENGTH_SHORT).show();
						}
						else
						{
							if(type.equals("error"))
							{
								Toast.makeText(con, jsonobject.getString("response"), Toast.LENGTH_SHORT).show();
							}else
							{
								//Toast.makeText(, "", duration)
							}
						}
					}
				}
			}
			catch(JSONException exc)
			{
				
			}
			
		}
	}
	
	private static String sendToServer(String data) throws Exception
	{
		String result="";
		InputStream stream;
		HttpClient client=new DefaultHttpClient();
		HttpPost post=new HttpPost("http://192.168.56.1:80/laravel-master/websites/test/cart/create");
		
		StringEntity entity=new StringEntity(data);
		
		post.setEntity(entity);
		
		HttpResponse response=client.execute(post);
		
		stream=response.getEntity().getContent();
		if(stream==null)
		{
			result="NO DATA RECEIVED FROM SERVER";
		}
		else
		{
			result=convertStreamToString(stream);
		}
		
		return result;
	}
	
	private class UploadingDataToServer extends AsyncTask<Object,Void,String>
	{
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(con,disptitle,"please wait...",true);
		}
		public String doInBackground(Object... objects)
		{
			String result="",uploaddata;
			
			if(objects[1].equals("user"))
			{
				if(!todelete)
				{
					resultid=accessuser.insertUser((User)objects[0]);
					if(resultid != -1)
					{
						User u=(User)objects[0];
						u.setId(resultid);
						prefs.storeUser(u);
					}
					uploaddata=createUserString((User)objects[0]);
				}
				else
				{
					uploaddata=createUserString((User)objects[0]);
					//accessuser.deleteUser((User)objects[0]);
				}
				
					
					
					if(hasIntent)
					{
						con.startActivity(i);
					}
					try
					{
						result=sendToServer(uploaddata);
					}
					catch(Exception exc)
					{
						Toast.makeText(con, "ERROR UPLOADING DATA", Toast.LENGTH_SHORT).show();
					}
					
				
				return result;
			}
			else
			{
				if(objects[1].equals("cart"))
				{
					accesscart.setCartStatus(1, (MyCart)objects[0]);
					uploaddata=createCartString((MyCart)objects[0]);
					if(hasIntent)
					{
						con.startActivity(i);
					}
					try
					{
						result=sendToServer(uploaddata);
					}
					catch(Exception exc)
					{
						Toast.makeText(con, "ERROR UPLOADING DATA", Toast.LENGTH_SHORT).show();
					}
					return result;
				}
				else
				{
					uploaddata=createProductString();
					try
					{
						result=sendToServer(uploaddata);
					}
					catch(Exception exc)
					{
						Toast.makeText(con, "ERROR READING PRODUCTS", Toast.LENGTH_SHORT).show();
					}
					return result;
				}
			}
		}
		
		public void onPostExecute(String str)
		{
			
			if(str.equals(""))
			{
				Toast.makeText(con, "NO DATA FROM SERVER", Toast.LENGTH_SHORT).show();
			}
			else
			{
				JSONObject object=null;
				try
				{
					object = new JSONObject(str);
				}
				catch(JSONException exc)
				{
					
				}
				parseJsonObject(object);
				
				/*if(!str.contains("inserted"))
				{
					extractProducts(str);
					Toast.makeText(con, "OKAY", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(con, str, Toast.LENGTH_SHORT).show();
				}*/
				
			}
			if(hasViews)
			{
				for(int i=0;i<viewlist.size();i++)
				{
					((TextView)viewlist.get(i)).setText("");
				}
			}
			
			dialog.dismiss();
		}
	}
}