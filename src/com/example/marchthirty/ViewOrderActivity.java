package com.example.marchthirty;

import android.app.*;
import android.os.*;
import android.view.View;
import  android.widget.*;
import android.content.*;

import java.text.SimpleDateFormat;
import java.util.*;

public class ViewOrderActivity extends Activity
{
	private int orderid,productid,userid,quantity,cartid;
	private long date;
	private Order order;
	private AccessDatabaseOrder accessorder;
	private AccessDatabaseUser accessuser;
	private AccessDatabaseProducts accessproduct;
	private String name;
	private Intent intent;
	private TextView titleview,nameview,userview,qtyview,cartview,dateview;
	private Button back;
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.view_order);
		accessorder = new AccessDatabaseOrder(ViewOrderActivity.this);
		accessuser= new AccessDatabaseUser(ViewOrderActivity.this);
		accessproduct = new AccessDatabaseProducts(ViewOrderActivity.this);
		
		accessorder.open();
		accessuser.open();
		accessproduct.open();
		
		titleview=(TextView)findViewById(R.id.view_order_orderid);
		nameview=(TextView)findViewById(R.id.view_order_productname);
		userview=(TextView)findViewById(R.id.view_order_username);
		qtyview=(TextView)findViewById(R.id.view_order_quantity);
		cartview=(TextView)findViewById(R.id.view_order_cartid);
		dateview=(TextView)findViewById(R.id.view_order_date);
		back = (Button)findViewById(R.id.view_order_back);
		
		intent=getIntent();
		
		orderid=intent.getIntExtra("orderid", -1);
		productid=intent.getIntExtra("productid", -1);
		cartid=intent.getIntExtra("cartid", -1);
		quantity=intent.getIntExtra("quantity", -1);
		userid=intent.getIntExtra("userid", -1);
		date=intent.getLongExtra("date", -1);
		
		titleview.setText(" "+orderid);
		if(productid == -1)
		{
			nameview.setText("No product name");
		}else
		{
			nameview.setText(accessproduct.getProductName(productid));
		}
		
		if(cartid == -1)
		{
			cartview.setText("No cart id");
		}else
		{
			cartview.setText("Cart id: "+cartid);
		}
		
		if(quantity == -1)
		{
			qtyview.setText("No quantity");
		}else
		{
			qtyview.setText("Quantity: "+quantity);
		}
		
		if(userid == -1)
		{
			userview.setText("No user name");
		}else
		{
			userview.setText("User: "+accessuser.getUserName(userid));
		}
		
		if(date == -1)
		{
			dateview.setText("No date");
		}else
		{
			dateview.setText("Date: "+convertToDate(date));
		}
		
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				accessorder.close();
				accessuser.close();
				accessproduct.close();
				
				Intent intent = new Intent(ViewOrderActivity.this,ListOrdersActivity.class);
				startActivity(intent);
			}
		});
	}
	
	private String getDateString(Date date)
	{
		SimpleDateFormat format=new SimpleDateFormat();
		return format.format(date);
	}
	
	private String convertToDate(Long date)
	{
		Date thedate=new Date(date);
		return getDateString(thedate);
	}
	
}