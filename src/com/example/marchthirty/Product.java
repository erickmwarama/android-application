package com.example.marchthirty;

public class Product
{
	private int productid,price;
	private String productname,weight;
	
	public String getWeight()
	{
		return this.weight;
	}
	
	public void setWeight(String w)
	{
		this.weight=w;
	}
	
	public int getProductId()
	{
		return this.productid;
	}
	
	public void setProductId(int i)
	{
		this.productid=i;
	}
	
	public String getProductName()
	{
		return this.productname;
	}
	
	public void setProductName(String s)
	{
		this.productname=s;
	}
	
	public int getPrice()
	{
		return this.price;
	}
	
	public void setPrice(int p)
	{
		this.price=p;
	}
	
	public String toString()
	{
		return this.productname;
	}
}