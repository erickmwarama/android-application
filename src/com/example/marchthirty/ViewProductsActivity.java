package com.example.marchthirty;

import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.os.*;

public class ViewProductsActivity extends FragmentActivity
{
	private ViewPager vp;
	private FragmentManager fm;
	private ViewPagerAdapter adapter;
	
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.products_viewpager);
		vp=(ViewPager) findViewById(R.id.viewpager);
		fm=getSupportFragmentManager();
		
		adapter=new ViewPagerAdapter(ViewProductsActivity.this,fm);
		vp.setAdapter(adapter);
	}
}