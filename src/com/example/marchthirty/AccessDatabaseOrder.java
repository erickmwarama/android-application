package com.example.marchthirty;

import android.content.*;
//import android.util.Log;
//import android.widget.Toast;
import android.database.sqlite.*;
import android.database.*;
import java.util.*;

public class AccessDatabaseOrder extends AccessDatabase
{
	private Context con;
	private AccessDatabaseCart accesscart;
	
	public AccessDatabaseOrder(Context c)
	{
		super(c);
		this.con=c;
		accesscart=new AccessDatabaseCart(con);
		accesscart.open();
	}
	
	public int insertOrder(Order or)
	{
		ContentValues cv=new ContentValues();
		cv.put("USERID",or.getUserid());
		cv.put("PRODUCTID",or.getProductId());
		cv.put("QUANTITY", or.getQuantity());
		cv.put("DATE", or.getDate());
		cv.put("CARTID", or.getCartId());
		
		return (int)writabledb.insert("ORDERS", null, cv);
		//Toast.makeText(con, "order inserted",Toast.LENGTH_SHORT).show();
		//Log.i("MARCH THIRTY","order succesfully made to the db");
	}
	
	public int getOrderId(Order or)
	{
		int value=-1;
		Cursor c=readabledb.query("ORDERS", new String[]{"_id"}, "USERID=? AND QUANTITY=? AND PRODUCTID=?", new String[]{new Integer(or.getUserid()).toString(),new Integer(or.getQuantity()).toString(),Integer.valueOf(or.getProductId()).toString()},null,null,null,null);
		if(c == null||c.getCount()==0)
		{
			return -1;
		}
		else
		{
			c.moveToFirst();
			while(!c.isAfterLast())
			{
				value=c.getInt(0);
				c.moveToNext();
			}
			
			return value;
		}
	}
	
	private Order convertCursorToOrder(Cursor c)
	{
		Order o=new Order();
		o.setOrderId(c.getInt(0));
		o.setUserid(c.getInt(1));
		o.setProductId(c.getInt(2));
		o.setQuantity(c.getInt(3));
		o.setDate(c.getLong(4));
		o.setCartId(c.getInt(5));
		return o;
	}
	
	public List<Order> getAllOrders()
	{
		List<Order> list=new ArrayList<Order>();
		Order o;
		Cursor cursor=readabledb.query("ORDERS", null, null, null, null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast())
		{
			o=convertCursorToOrder(cursor);
			list.add(o);
			cursor.moveToNext();
		}
		return list;
	}
	
	public List<Order> getUserOrders(int userid)
	{
		List<Order> orders=new ArrayList<Order>();
		Order o;
		Cursor cur=readabledb.query("ORDERS", null, "USERID=?", new String[]{Integer.valueOf(userid).toString()},null,null,null,null);
		if(cur!=null)
		{
			cur.moveToFirst();
			while(!cur.isAfterLast())
			{
				o=convertCursorToOrder(cur);
				orders.add(o);
				cur.moveToNext();
			}
		}
		return orders;
	}
	
	public Order getOrder(int orderid)
	{
		Order order=null;
		Cursor cursor=readabledb.query("ORDERS", null, "_id=?", new String[]{Integer.valueOf(orderid).toString()}, null, null, null, null);
		if(cursor != null && cursor.getCount()==1)
		{
			cursor.moveToFirst();
			while(!cursor.isAfterLast())
			{
				order=convertCursorToOrder(cursor);
				cursor.moveToNext();
			}
		}
		return order;
	}
	
	public List<Order> getCartOrders(int cartid)
	{
		List<Order> orders=new ArrayList<Order>();
		Order order;
		if(accesscart.checkCartStatus(cartid))
		{
			Cursor cursor=readabledb.query("ORDERS", null, "CARTID=?", new String[]{Integer.valueOf(cartid).toString()}, null, null, null, null);
			if(cursor != null)
			{
				cursor.moveToFirst();
				while(!cursor.isAfterLast())
				{
					order=convertCursorToOrder(cursor);
					orders.add(order);
					cursor.moveToNext();
				}
			}
		}
		
		return orders;
	}
}