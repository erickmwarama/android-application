package com.example.marchthirty;

import android.support.v4.app.*;
import android.view.*;
import android.os.*;
import android.content.*;
import java.util.*;
import android.widget.*;

public class ProductsByNameFragment extends Fragment
{
	private Context context;
	private List<Product> products;
	private TextView nameview;
	private ListView listview;
	private String thename;
	private AccessDatabaseProducts accessproducts;
	private ProductsByNameListviewAdapter listadapter;
	
	public ProductsByNameFragment(Context con,String name)
	{
		this.context=con;
		accessproducts = new AccessDatabaseProducts(context);
		accessproducts.open();
		this.products=accessproducts.getProductsByName(name);
		this.thename=name;
		listadapter= new ProductsByNameListviewAdapter(context,products);
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup group,Bundle bundle)
	{
		View view=inflater.inflate(R.layout.products_by_name, group,false);
		nameview=(TextView)view.findViewById(R.id.products_by_name_name);
		nameview.setText(products.get(0).getProductName());
		listview=(ListView)view.findViewById(R.id.products_by_name_list);
		
		nameview.setText(thename);
		listview.setAdapter(listadapter);
		
		return view;
	}
}