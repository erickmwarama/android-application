package com.example.marchthirty;

import android.support.v4.app.Fragment;
import android.app.*;
import android.view.*;
import android.widget.*;
import android.os.*;
import android.content.*;
import android.graphics.Color;

public class ListOfProductsFragment extends Fragment
{
	private ListView list;
	private AccessDatabaseProducts accessproducts;
	private FragmentManager manager;
	private Activity context;
	
	private ListOfProductsFragmentAdapter adapter;
	
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		context=activity;
		
	}
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		accessproducts=new AccessDatabaseProducts(context);
		accessproducts.open();
		adapter = new ListOfProductsFragmentAdapter(context);
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup group,Bundle bundle)
	{
		View view=inflater.inflate(R.layout.list_fragment, group,false);
		
		list=(ListView)view.findViewById(R.id.list_fragment);
		list.setAdapter(adapter);
		list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		selectItem(0);
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> adapter,View view,int pos,long l)
			{
				manager=context.getFragmentManager();
				FragmentTransaction transaction = manager.beginTransaction();
				//transaction.replace(R.id.list_products_detailsfragment, new ListOfProductDetailsFragment(pos+1));
				list.setItemChecked(pos, false);
				transaction.commit();
			}
		});
		
		/*list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View view,
					int pos, long arg3) {				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				
				
			}
		});*/
		
		return view;
	}
	
	public void selectItem(int index)
	{
		list.setItemChecked(index, true);
	}
	
}