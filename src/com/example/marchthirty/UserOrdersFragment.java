package com.example.marchthirty;

import android.app.*;
import android.os.*;
import android.view.*;
import java.util.*;
import android.content.*;
import android.widget.*;

public class UserOrdersFragment extends Fragment
{
	//private List<Order> orders;
	//private AccessDatabaseOrder accessorders;
	private  AccessDatabaseUser accessuser;
	private Activity activity;
	private String name;
	private UserOrdersListViewAdapter adapter;
	private int user;
	
	public UserOrdersFragment(){}
	
	public UserOrdersFragment(int userid)
	{
		this.user=userid;
	}
	
	public void onAttach(Activity act)
	{
		super.onAttach(act);
		this.activity=act;
	}
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		
		if(bundle != null)
		{
			this.user = bundle.getInt("userid");
		}
		accessuser = new AccessDatabaseUser(activity);
		accessuser.open();
		name= accessuser.getUserName(user);
		adapter = new UserOrdersListViewAdapter(activity,user);
				
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup group,Bundle bundle)
	{
		super.onCreateView(inflater, group, bundle);
		
		
		View view = inflater.inflate(R.layout.userordersfragment, group,false);
		TextView title=(TextView)view.findViewById(R.id.userorders_username);
		ListView listview =(ListView)view.findViewById(R.id.userorders_list);
		TextView total =(TextView)view.findViewById(R.id.userorders_disptotal);
		
		title.setText("ORDERS MADE BY USER "+name);
		listview.setAdapter(adapter);
		total.setText(adapter.getTotalCost()+"");
		return view;
	}
	
	public void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putInt("userid", user);
		bundle.putString("name", name);
	}
	
	
}