package com.example.marchthirty;

import android.widget.*;
import android.content.*;

import java.util.*;
//import java.util.regex.*;
import android.database.*;
import android.view.*;

public class ProductNameAdapter extends CursorAdapter
{
	
	public List<Product> products;
	
	//private Context con;
	private AccessDatabaseProducts accessproducts;
	private TextView nameview,priceview,weightview;
		
	@SuppressWarnings("deprecation")
	public ProductNameAdapter(Context context,Cursor cursor)
	{
		super(context,cursor);
		accessproducts= new AccessDatabaseProducts(context);
		accessproducts.open();
	}


	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		nameview=(TextView)view.findViewById(R.id.order_list_name);
		priceview=(TextView)view.findViewById(R.id.order_list_price);
		weightview=(TextView)view.findViewById(R.id.order_list_weight);
		
		nameview.setText(cursor.getString(1));
		priceview.setText("price: "+cursor.getInt(2)+"");
		weightview.setText("weight: "+cursor.getString(3));
		
	}


	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		LayoutInflater inflater=LayoutInflater.from(context);
		View row=inflater.inflate(R.layout.make_order_list_items, parent,false);
		
		return row;
	}
	
	public Cursor runQueryOnBackgroundThread(CharSequence seq)
	{
		Cursor cursor=null;
		if(getFilterQueryProvider() != null)
		{
			return getFilterQueryProvider().runQuery(seq);
		}
		
		cursor=accessproducts.getMatchingProducts(seq.toString());
		return cursor;
	}
	
	public String convertToString(Cursor cursor)
	{
		if(cursor != null)
		{
			return cursor.getString(1);
		}
		else
		{
			return "";
		}
		
	}
	
	
	/*public ProductNameAdapter(Context context,List<Product> list,View v)
	{
		super(context,R.layout.make_order_list_items,list);
		this.autocomplete=(AutoCompleteTextView)v;
		this.con=context;
		this.products=list;
		
	}
	
	public View getView(int pos,View v,ViewGroup vg)
	{
	
		 c=0;
		LayoutInflater inflater = (LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row=inflater.inflate(R.layout.make_order_list_items, vg,false);
		//parent=(LinearLayout)row.findViewById(R.id.order_list_parent);
		nameview=(TextView)row.findViewById(R.id.order_list_name);
		priceview=(TextView)row.findViewById(R.id.order_list_price);
		weightview=(TextView)row.findViewById(R.id.order_list_weight);
		
		for(int i=0;i<products.size();i++)
		{
			Pattern pattern = Pattern.compile("\\b"+autocomplete.getText().toString());
			Matcher matcher=pattern.matcher(products.get(i).getProductName());
			if(matcher.find())
			{
				names[c]=products.get(i).getProductName();
				prices[c]=products.get(i).getPrice();
				weights[c]=products.get(i).getWeight();
				c+=1;	
			}
		}
		
		nameview.setText(names[pos]);
		priceview.append(prices[pos]+"");
		weightview.append(weights[pos]);
		
		return row;
	}*/
}