package com.example.marchthirty;

import android.content.*;
import android.database.sqlite.*;
import android.database.*;
import android.util.Log;
import android.widget.Toast;
import java.util.*;

public class AccessDatabaseProducts extends AccessDatabase
{
	private Context con;
	
	public AccessDatabaseProducts(Context c)
	{
		super(c);
		this.con=c;
	}
	
	public boolean insertProduct(Product pro)
	{
		ContentValues cv=new ContentValues();
		cv.put("productname", pro.getProductName());
		cv.put("price", pro.getPrice());
		cv.put("weight", pro.getWeight());
		writabledb.insert("PRODUCTS", null, cv);
		
		//Toast.makeText(con,"PRODUCT INSERTED",Toast.LENGTH_SHORT).show();
		return true;
	}
	
	public boolean insertProducts(List<Product> list)
	{
		for(int i=0;i<list.size();i++)
		{
			Product product = list.get(i);
			ContentValues cv = new ContentValues();
			cv.put("productname", product.getProductName());
			cv.put("price", product.getPrice());
			cv.put("weight", product.getWeight());
			cv.put("serverid", product.getProductId());
			
			writabledb.insert("PRODUCTS",null,cv);
		}
		
		return true;
	}
	
	public int getProductId(Product pro)
	{
		Cursor c=readabledb.query("PRODUCTS", new String[]{"_id"}, "PRODUCTNAME=?", new String[]{pro.getProductName()}, null, null, null);
		if(c!=null && c.getCount()==1)
		{
			c.moveToFirst();
			while(!c.isAfterLast())
			{
				//return c.getInt(0);
				c.moveToNext();
			}
			return c.getInt(0);
		}
		else
		{
			return -1;
		}
	}
	
	private Product convertCursorToProduct(Cursor c)
	{
		Product pro=new Product();
		pro.setProductId(c.getInt(0));
		pro.setProductName(c.getString(1));
		pro.setPrice(c.getInt(2));
		pro.setWeight(c.getString(3));
		return pro;
	}
	
	public List<Product> getAllProducts()
	{
		Product p=new Product();
		List<Product> list=new ArrayList<Product>();
		Cursor c=readabledb.query("PRODUCTS", null, null, null, null, null, null);
		if(c != null)
		{
			c.moveToFirst();
			while(!c.isAfterLast())
			{
				p=convertCursorToProduct(c);
				list.add(p);
				c.moveToNext();
			}
		}
		return list;
	}
	
	private String convertCursorToString(Cursor cur)
	{
		String s=cur.getString(0);
		return s;
	}
	
	public List<String> getAllProductNames()
	{
		String s;
		List<String> names=new ArrayList<String>();
		Cursor c=readabledb.query("PRODUCTS", new String[]{"PRODUCTNAME"}, null, null, null, null, null);
		if(c != null)
		{
			c.moveToFirst();
			while(!c.isAfterLast())
			{
				s=convertCursorToString(c);
				if(!names.contains(s))
				{
					names.add(s);
				}
				
				c.moveToNext();
			}
		}
		return names;
	}
	
	public boolean deleteProduct(int productid)
	{
		writabledb.delete("PRODUCTS", "_id = ?", new String[]{Integer.valueOf(productid).toString()});
		return true;
	}
	
	public int getProductId(String str)
	{
		Cursor c=readabledb.query("PRODUCTS", new String[]{"_id"}, "PRODUCTNAME=?", new String[]{str}, null, null, null);
		if(c!=null && c.getCount()==1)
		{
			c.moveToFirst();
			
			return c.getInt(0);
		}
		else
		{
			return -1;
		}
	}
	
	public String getProductName(int id)
	{
		String name;
		Cursor cursor=readabledb.query("PRODUCTS", new String[]{"PRODUCTNAME"}, "_id=?", new String[]{Integer.valueOf(id).toString()}, null, null, null, null);
		if(cursor !=null && cursor.getCount()==1)
		{
			cursor.moveToFirst();
			name=cursor.getString(0);
			return name;
		}
		return null;
	}
	
	public Product getProduct(int id)
	{
		//Log.e("access products",""+id);
		Cursor cursor=readabledb.query("PRODUCTS", null, "_id=?", new String[]{Integer.valueOf(id).toString()}, null, null, null, null);
		if(cursor != null)
		{
			cursor.moveToFirst();
			Product pro=convertCursorToProduct(cursor);
			return pro;
		}
		return null;
	}
	
	public int getProductId(String name,String w,int p)
	{
		Cursor c=readabledb.query("PRODUCTS", new String[]{"_id"}, "PRODUCTNAME=? AND WEIGHT=? AND PRICE=?", new String[]{name,w,Integer.valueOf(p).toString()}, null, null, null);
		if(c!=null)
		{
			c.moveToFirst();
			
			return c.getInt(0);
		}
		else
		{
			return -1;
		}
	}
	
	public Cursor getMatchingProducts(String string)
	{
		String sql="SELECT * FROM PRODUCTS WHERE PRODUCTNAME LIKE '%"+string+"%'";
		Cursor cursor=readabledb.rawQuery(sql, null);
		
		return cursor;
	}
	
	public Map<String,List<Product>> getProductsByName()
	{
		Map<String,List<Product>> theproducts = new HashMap<String,List<Product>>();
		List<Product> list;
		String name;
		Cursor cursor=readabledb.query("PRODUCTS", null, null, null, null, null, null, null);
		if(cursor != null)
		{
			cursor.moveToFirst();
			while(!cursor.isAfterLast())
			{
				Product product=convertCursorToProduct(cursor);
				name=product.getProductName();
				if(theproducts.containsKey(name))
				{
					list=theproducts.get(name);
					list.add(product);
					theproducts.put(name, list);
				}
				else
				{
					list=new ArrayList<Product>();
					list.add(product);
					theproducts.put(name, list);
				}
				cursor.moveToNext();
			}
		}
		
		return theproducts;
	}
	
	public List<Product> getProductsByName(String name)
	{
		List<Product> products= new ArrayList<Product>();
		Cursor cursor=readabledb.query("PRODUCTS", null, "PRODUCTNAME = ?", new String[]{name}, null, null, null);
		if(cursor != null)
		{
			cursor.moveToFirst();
			while(!cursor.isAfterLast())
			{
				Product product=convertCursorToProduct(cursor);
				products.add(product);
				cursor.moveToNext();
			}
		}
		
		return products;
	}
	
	public int getProductCount()
	{
		Cursor cursor=readabledb.query("PRODUCTS", null, null, null, null, null, null, null);
		if(cursor != null)
		{
			return cursor.getCount();
		}
		return -1;
	}
	
	public int getProductByNameCount()
	{
		List<String> list=new ArrayList<String>();
		Cursor cursor=readabledb.query("PRODUCTS", null, null, null, null, null, null, null);
		if(cursor != null)
		{
			cursor.moveToFirst();
			while(!cursor.isAfterLast())
			{
				Product product=convertCursorToProduct(cursor);
				if(!list.contains(product.getProductName()))
				{
					list.add(product.getProductName());
				}
				else
				{
					
				}
				cursor.moveToNext();
			}
			return list.size();
		}
		return 0;
	}
}