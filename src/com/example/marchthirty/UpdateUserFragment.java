package com.example.marchthirty;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.util.Log;
import android.view.*;
import android.widget.*;

public class UpdateUserFragment extends Fragment
{
	private int userid;
	public  Activity activity;
	private AccessDatabaseUser accessuser;
	private User user;
	private MyPreferences mypref;
	
	public UpdateUserFragment(int user)
	{
		this.userid = user;
	}
	
	public  void onAttach(Activity act)
	{
		super.onAttach(act);
		this.activity = act;
	}
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		accessuser = new AccessDatabaseUser(activity);
		accessuser.open();
		user = accessuser.getUser(userid);
		mypref = new MyPreferences(activity);
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup parent,Bundle bundle)
	{
		View view = inflater.inflate(R.layout.update_user_activity, parent,false);
		TextView title =(TextView) view.findViewById(R.id.update_title);
		final EditText name_txt = (EditText)view.findViewById(R.id.update_name_text);
		final EditText email_txt = (EditText)view.findViewById(R.id.update_email_text);
		final EditText pass_txt = (EditText)view.findViewById(R.id.update_pass_text);
		Button btn = (Button)view.findViewById(R.id.update_btn_update);
		
		title.setText("update details for user number: "+user.getId());
		name_txt.setText(user.getFullname());
		email_txt.setText(user.getEmail());
		pass_txt.setText(user.getPassword());
		
		final Fragment fg = this;
		
		btn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				String name,email,password;
				name=name_txt.getText().toString();
				email=email_txt.getText().toString();
				password=pass_txt.getText().toString();
				
				user.setEmail(email);
				user.setFullname(name);
				user.setPassword(password);
				
				if(checkConnection())
				{
					new UpdateUser().execute(user);
				}
				else
				{
					Toast.makeText(activity, "sorry you do not have internet connection",Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		return view;
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }


private static String parseJson(String string)
{
	String str="";
	JSONObject object = null;
	try
	{
		object=new JSONObject(string);
		str=object.getString("response");
	}
	catch(JSONException exc)
	{
		
	}
	return str;
}

private static String convertStreamToString(InputStream stream) throws Exception
{
	StringBuffer buffer=new StringBuffer();
	String line;
	BufferedReader reader=new BufferedReader(new InputStreamReader(stream));
	while((line=reader.readLine()) != null)
	{
		buffer.append(line);
	}
	return buffer.toString();
}

private static String sendToServer(String string) throws Exception
{
	InputStream stream;
	HttpClient client=new DefaultHttpClient();
	HttpPost post = new HttpPost(NetworkAddress.updateuser);
	StringEntity entity=new StringEntity(string);
	post.setEntity(entity);
	
	HttpResponse response=client.execute(post);
	stream=response.getEntity().getContent();
	return convertStreamToString(stream);
}

private static String createUserString(User user)
{
	JSONObject json=new JSONObject();
	try
	{
		//json.put("object","update");
		json.put("userid", user.getId());
		json.put("fullname", user.getFullname());
		json.put("email", user.getEmail());
		json.put("password", user.getPassword());
	}
	catch(JSONException exc)
	{
		
	}
	return json.toString();
}


private class UpdateUser extends AsyncTask<User,Void,String>
{
	private String data="",result="",response="";
	//private User user;
	private ProgressDialog dialog;
	
	public void onPreExecute()
	{
		dialog=ProgressDialog.show(activity, "updating", "please wait...");
	}
	
	public String doInBackground(User... users)
	{
		data=createUserString(users[0]);
		
		
			if(accessuser.updateUser(users[0]))
			{
				User user = mypref.retreiveUser();
				if(user.getId() == users[0].getId())
				{
					mypref.storeUser(users[0]);
				}
				try
				{
					result=sendToServer(data);
				}
				catch(Exception exc)
				{
					Log.i("march thirty",exc.getMessage());
				}
			
				response= parseJson(result);
				
				
				activity.runOnUiThread(new Runnable()
				{
					public void run()
					{
						Toast.makeText(activity, response, Toast.LENGTH_SHORT).show();
					}
				});
				
				
			}
			else
			{
				activity.runOnUiThread(new Runnable()
				{
					public void run()
					{
						Toast.makeText(activity, "unable to update user", Toast.LENGTH_SHORT).show();
					}
				});
			}
	
			activity.getFragmentManager().popBackStack("name",0);
		return null;
	}
	
	public void onPostExecute(String string)
	{
		
		dialog.dismiss();
		
	}
}
}