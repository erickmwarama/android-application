package com.example.marchthirty;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import java.util.*;
import android.content.*;

public class ListProductsActivity extends Activity
{
	private GridView productsview;
	private List<Product> thelist;
	private AccessDatabaseProducts accessproducts;
	private ListProductsAdapter adapter;
	private Context context;
	private ActionBar actionbar;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.products_grid);
		productsview=(GridView)findViewById(R.id.products_grid_products);
		
		context=ListProductsActivity.this;
		accessproducts = new AccessDatabaseProducts(ListProductsActivity.this);
		accessproducts.open();
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		thelist=accessproducts.getAllProducts();
		adapter=new ListProductsAdapter(context,thelist);
		productsview.setAdapter(adapter);
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(context,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(context,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(context,ListProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(context,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(context,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(context,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(context,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
	
	public void onStop()
	{
		super.onStop();
		accessproducts.close();
		thelist = null;
	}
	
	public void onRestart()
	{
		super.onRestart();
		accessproducts.open();
		thelist = accessproducts.getAllProducts();
	}
}