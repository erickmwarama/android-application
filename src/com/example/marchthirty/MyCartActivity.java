package com.example.marchthirty;

import java.io.*;
import java.util.*;
import android.util.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;

import android.app.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;


public class MyCartActivity extends Activity
{
	//private ListView cartview;
	private ExpandableListView expandablelist;
	private Button cartorder;
	private TextView carttitle;
	//private MyCartAdapter adapter;
	private Activity con;
	private AccessDatabaseCart accesscart;
	private MyPreferences myprefs;
	private ActionBar actionbar;
	private MyCart cart;
	private CartListAdapter adapter;
	//private static List<Order> orders;
	//private Boolean noorders;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.my_cart_activity);
		con=MyCartActivity.this;
		//cartview=(ListView)findViewById(R.id.cartview);
		cartorder=(Button)findViewById(R.id.cartorder);
		cartorder.setBackgroundResource(R.drawable.bluebackground);
		carttitle=(TextView)findViewById(R.id.mycartactivity_cartid);
		
		expandablelist = (ExpandableListView)findViewById(R.id.cart_list);
		
		accesscart=new AccessDatabaseCart(con);
		
		accesscart.open();
		
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		myprefs=new MyPreferences(con);
		if(myprefs.retreiveUser().getId()== -1)
		{
			Intent intent=new Intent(con,MainActivity.class);
			Toast.makeText(con,"YOU NEED TO LOGIN",Toast.LENGTH_SHORT).show();
			startActivity(intent);
		}
		else
		{
			cart = accesscart.getCart(myprefs.retreiveUser().getId());
			
			if(cart == null)
			{
				carttitle.setText("YOU HAVE NO ORDERS IN YOUR CART");
				cartorder.setText("CLICK TO MAKE ORDERS");
				cartorder.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						accesscart.close();
						
						Intent intent=new Intent(con,CreateOrderActivity.class);
						startActivity(intent);
					}
				});
			}
			else
			{
				//carttitle.append(cart.getCartId()+"");
				//adapter=new MyCartAdapter(con,cart);
				//cartview.setAdapter(adapter);
				adapter = new CartListAdapter(con,cart);
				expandablelist.setAdapter(adapter);
			
				cartorder.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						
						if(checkConnection())
						{
							new UploadCart().execute(cart);
						}
						else
						{
							Toast.makeText(con, "sorry. you do not have internet connection", Toast.LENGTH_SHORT).show();
						}
						
						
					}
				});
			}
			
		}
		
		
	}
	
	public void onPause()
	{
		super.onPause();
		accesscart.close();
		//accessorder.close();
	}
	
	public void onResume()
	{
		super.onResume();
		accesscart.open();
		//accessorder.open();
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(con,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(con,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(con,ListOfProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(con,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(con,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(con,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(con,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }
	
	private static String parseJson(String string)
	{
		String result="";
		try
		{
			JSONObject object=new JSONObject(string);
			result= object.getString("response");
		}
		catch(Exception exc)
		{
			
		}
		return result;
	}
	
	private static String convertStreamToString(InputStream stream) throws Exception
	{
		String line;
		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		while((line=reader.readLine()) != null)
		{
			buffer.append(line);
		}
		
		return buffer.toString();
	}
	
	private static String sendCartToServer(String cart) throws Exception
	{
		HttpClient client= new DefaultHttpClient();
		HttpPost post = new HttpPost(NetworkAddress.createcart);
		StringEntity entity = new StringEntity(cart);
		post.setEntity(entity);
		
		HttpResponse response = client.execute(post);
		InputStream stream=response.getEntity().getContent();
		
		return convertStreamToString(stream);
	}
	
	private static String createCartString(MyCart cart)
	{
		JSONObject object= new JSONObject();
		List<Order> orders;
		orders=cart.getOrders();
		try
		{
			for(int i=0;i<orders.size();i++)
			{
				Order order=orders.get(i);
				object.accumulate("userid", order.getUserid());
				object.accumulate("cartid", order.getCartId());
				object.accumulate("orderid", order.getOrderId());
				object.accumulate("productid", order.getProductId());
				object.accumulate("quantity", order.getQuantity());
				object.accumulate("date", order.getDate());
			}
			
		}
		catch(JSONException exc)
		{
			Log.e("my cart activity",exc.getMessage());
		}
		Log.i("march thirty",object.toString());
		return object.toString();
	}
	
	private class UploadCart extends AsyncTask<MyCart,Void,String>
	{
		private ProgressDialog dialog;
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(con, "uploading cart", "please wait...");
		}
		
		public String doInBackground(MyCart... carts)
		{
			String cartstring="",result="";
			cartstring=createCartString(carts[0]);
			

				try
				{
					result=sendCartToServer(cartstring);
				}catch(Exception exc)
				{
					Log.i("march thitry",exc.getMessage());
				}
				
					final String res=parseJson(result);
					if(res.equals(""))
					{
						con.runOnUiThread(new Runnable()
						{
							public void run()
							{
								Toast.makeText(con, "ERROR SENDING CART TO SERVER", Toast.LENGTH_SHORT).show();
							}
						});
					}
					else
					{
						con.runOnUiThread(new Runnable()
						{
							public void run()
							{
								Toast.makeText(con, res, Toast.LENGTH_SHORT).show();
							}
						});
						
						accesscart.setCartStatus(1, carts[0]);
					}
			
			
				accesscart.close();
				//accessorder.close();
				Intent intent=new Intent(con,CreateOrderActivity.class);
				startActivity(intent);
			
						
			
			return null;
		}
		
		public void onPreExecute(String string)
		{
			dialog.dismiss();
		}
	}
}