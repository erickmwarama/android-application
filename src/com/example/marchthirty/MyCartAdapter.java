package com.example.marchthirty;

import android.widget.*;
import android.content.*;
import java.util.*;
import android.view.*;

public class MyCartAdapter extends BaseAdapter
{
	private Context context;
	private List<Order> theorders;
	private AccessDatabaseProducts accessproducts;
	private TextView weightview,nameview,qtyview,priceview,totalview;
	private int orderid,totalprice;
	
	public MyCartAdapter(Context con, MyCart cart)
	{
		//super(con,R.layout.mycartviews,cart.getOrders());
		super();
		this.context=con;
		this.theorders=cart.getOrders();
		accessproducts=new AccessDatabaseProducts(con);
		accessproducts.open();
	}
	
	public View getView(int pos,View convertView,ViewGroup vg)
	{
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Order order=(Order)getItem(pos);
		Product theproduct=accessproducts.getProduct(order.getProductId());
		
		View view=convertView;
		
		if(view == null)
		{
			view=inflater.inflate(R.layout.mycartviews,vg,false);
		}
		
		weightview=(TextView)view.findViewById(R.id.mycart_productweight);
		nameview=(TextView)view.findViewById(R.id.mycart_productname);
		qtyview=(TextView)view.findViewById(R.id.mycartquantity);
		priceview=(TextView)view.findViewById(R.id.mycartprice);
		totalview=(TextView)view.findViewById(R.id.mycart_displaytotal);
		
		orderid=order.getOrderId();
		nameview.append(theproduct.getProductName());
		weightview.append(theproduct.getWeight());
		qtyview.append(order.getQuantity()+"");
		priceview.append(theproduct.getPrice()+"");
		totalprice=theproduct.getPrice()*order.getQuantity();
		totalview.setText(totalprice+"");
		
		return view;
	}

	@Override
	public int getCount() {
		
		return theorders.size();
	}

	@Override
	public Object getItem(int pos) {
		
		return theorders.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return theorders.get(pos).getOrderId();
	}
	
}