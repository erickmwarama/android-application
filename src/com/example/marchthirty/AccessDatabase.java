package com.example.marchthirty;

import android.database.sqlite.*;
import android.content.*;
//import java.util.*;
//import android.util.*;

public class AccessDatabase
{
	
	private TheDatabase thedb;
	protected SQLiteDatabase readabledb,writabledb;
	private Context con;
	
	
	public AccessDatabase(Context c)
	{
		this.con=c;
		this.thedb=new TheDatabase(con);
	}
		
	public void open()
	{
		this.thedb=new TheDatabase(con);
		this.readabledb=thedb.getReadableDatabase();
		this.writabledb=thedb.getWritableDatabase();
	}
	
	public void close()
	{
		this.thedb.close();
		this.readabledb.close();
		this.writabledb.close();
	}
	
}



