package com.example.marchthirty;

import android.app.*;
import android.view.*;
import android.widget.*;
import android.os.*;
import android.content.*;
import android.support.v4.app.Fragment;

public class ListOfProductDetailsFragment extends Fragment
{
	//private ListView list;
	private AccessDatabaseProducts accessproducts;
	
	private TextView detailstitle,productname,nameview,id,weight,price;
	private ImageView image;
	private Button button;
	private int index;
	private Product product;
	private setNameInterface theactivity;
	
	public interface setNameInterface
	{
		public void setName(String name);
	}
	
	public ListOfProductDetailsFragment()
	{
		
	}
	
	public ListOfProductDetailsFragment(int pos)
	{
		this.index=pos;
	}
	
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		try
		{
			theactivity=(setNameInterface)activity;
		}
		catch(ClassCastException exc)
		{
			Toast.makeText(activity, "activity doesn't implement setNameInterface", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		if(bundle != null)
		{
			index=bundle.getInt("index");
		}
		accessproducts=new AccessDatabaseProducts((Context)theactivity);
		accessproducts.open();
		product = accessproducts.getProduct(index);
		//adapter = new ListOfProductDetailsAdapter(context,index);
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup group,Bundle bundle)
	{
		View view=inflater.inflate(R.layout.products_by_name_list, group,false);
		
		id=(TextView)view.findViewById(R.id.products_by_name_list_id);
		weight=(TextView)view.findViewById(R.id.products_by_name_list_weight);
		price=(TextView)view.findViewById(R.id.products_by_name_list_price);
		image=(ImageView)view.findViewById(R.id.products_by_name_list_image);
		button=(Button)view.findViewById(R.id.products_by_name_button);
		
		button.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				theactivity.setName(product.getProductName());
				
			}
		});
		
		id.setText("Id: "+product.getProductId());
		weight.setText("Weight: "+product.getWeight());
		price.setText("Price: "+product.getPrice());
		
		setImage(image);
		return view;
	}
	
	public void onStop()
	{
		super.onStop();
		accessproducts.close();
	}
	
	public void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putInt("index", index);
	}
	
	/*public void onResume()
	{
		super.
	}*/
	
	private void setImage(ImageView image)
	{
		switch(product.getProductId())
		{
		case 1:
		case 21:
			image.setImageResource(R.drawable.pineapples);
			break;
		case 2:
			image.setImageResource(R.drawable.apples);
			break;
		case 3:
		case 22:
			image.setImageResource(R.drawable.mangoes);
			break;
		case 4:
		case 24:
			image.setImageResource(R.drawable.oranges);
			break;
		case 5:
			image.setImageResource(R.drawable.lemons);
			break;
		case 6:
		case 29:
			image.setImageResource(R.drawable.water_melons);
			break;
		case 7:
			image.setImageResource(R.drawable.guavas);
			break;
		case 8:
			image.setImageResource(R.drawable.pears);
			break;
		case 9:
		case 25:
			image.setImageResource(R.drawable.peaches);
			break;
		case 10:
		case 28:
			image.setImageResource(R.drawable.bananas);
			break;
		case 11:
			image.setImageResource(R.drawable.strawberries);
			break;
		case 12:
		case 30:
			image.setImageResource(R.drawable.blackcurrant);
			break;
		case 13:
			image.setImageResource(R.drawable.passionfruit);
			break;
		case 14:
			image.setImageResource(R.drawable.tomatoes);
			break;
		case 15:
		case 27:
			image.setImageResource(R.drawable.carrots);
			break;
		case 16:
		case 23:
			image.setImageResource(R.drawable.cassava);
			break;
		case 17:
			image.setImageResource(R.drawable.pepper);
			break;
		case 18:
			image.setImageResource(R.drawable.sweet_potatoes);
			break;
		case 19:
		case 26:
			image.setImageResource(R.drawable.pawpaw);
			break;
		case 20:
			image.setImageResource(R.drawable.avocado);
			break;
		default:
			break;
	}
	}

}