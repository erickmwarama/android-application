package com.example.marchthirty;

import com.example.marchthirty.ListOfProductDetailsFragment.setNameInterface;
import android.widget.*;
//import android.app.*;
import android.os.*;
import android.support.v4.view.*;
import android.support.v4.app.*;

public class ListOfProductsActivity extends FragmentActivity implements setNameInterface
{
	//private Fragment listfragment;
	private FragmentManager manager;
	private TextView productname,nameview;
	private ViewPager pager;
	private ListOfProductsPagerAdapter adapter;
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.list_products_activity_with_fragments);
		productname=(TextView)findViewById(R.id.list_products_prodname);
		nameview=(TextView)findViewById(R.id.list_products_name);
		pager=(ViewPager)findViewById(R.id.list_products_pager);
		
		if(bundle != null)
		{
			productname.setText(bundle.getString("product"));
			nameview.setText(bundle.getString("name"));
		}
		
		manager=getSupportFragmentManager();
		final ListOfProductsFragment listfragment=new ListOfProductsFragment();
		FragmentTransaction transaction=manager.beginTransaction();
		transaction.replace(R.id.list_products_listfragment, listfragment);
		//transaction.replace(R.id.list_products_detailsfragment, new ListOfProductDetailsFragment());
		transaction.commit();
		
		adapter=new ListOfProductsPagerAdapter(ListOfProductsActivity.this,manager);
		pager.setAdapter(adapter);
		//listfragment.selectItem(0);
		
		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int pos) {
				listfragment.selectItem(pos);
				
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
				
			}
		});
	}

	@Override
	public void setName(String name) {
		nameview.setText("Name: ");
		productname.setText(name);
	}
	
	public void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putString("name", nameview.getText().toString());
		bundle.putString("product", productname.getText().toString());
	}
}