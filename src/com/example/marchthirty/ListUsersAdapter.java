package com.example.marchthirty;

import java.util.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;
import java.io.*;
import android.widget.*;
import android.app.*;
import android.content.*;
import android.util.Log;
import android.view.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;

public class ListUsersAdapter extends ArrayAdapter<User>
{
	private List<User> thelist;
	public Activity con;
	private TextView idview,nameview,emailview,pswdview;
	private Button delete,update;
	private AccessDatabaseUser accessuser;
	public ProgressDialog dialog;
	
	public ListUsersAdapter(Context c,List<User> list)
	{
		super(c,R.layout.userlist,list);
		this.thelist=list;
		this.con=(Activity)c;
		accessuser=new AccessDatabaseUser(con);
		accessuser.open();
		//Handler handler= new Handler();
	}
	
	public View getView(int pos,View convertView,ViewGroup vg)
	{
		LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final User theuser = thelist.get(pos);
		
		View row=convertView;
		
		if(row== null)
		{
			row=inflater.inflate(R.layout.userlist, vg,false);
		}
		
		idview=(TextView)row.findViewById(R.id.userlistid);
		nameview=(TextView)row.findViewById(R.id.userlistname);
		emailview=(TextView)row.findViewById(R.id.userlistemail);
		pswdview=(TextView)row.findViewById(R.id.userlistpassword);
		delete=(Button)row.findViewById(R.id.userlist_delete);
		update=(Button)row.findViewById(R.id.userlist_update);
		
		idview.setText(theuser.getId()+"");
		nameview.setText(theuser.getFullname());
		emailview.setText(theuser.getEmail());
		pswdview.setText(theuser.getPassword());
		
		update.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent=new Intent(con,UpdateUserActivity.class);
				intent.putExtra("userid", theuser.getId());
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				con.startActivity(intent);
			}
		});
		
		delete.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0)
			{
				AlertDialog.Builder builder=new AlertDialog.Builder(con);
				builder.setTitle("DELETE USER");
				builder.setMessage("do you want to delete user: "+theuser.getFullname());
				builder.setPositiveButton("delete", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						
						if(checkConnection())
						{
							new DeleteUser().execute(theuser.getId());
						}
						else
						{
							Toast.makeText(con, "sorry. you do not have internet connection", Toast.LENGTH_SHORT).show();
						}
						
						
					}
				});
				
				builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						
						
					}
				});
			AlertDialog dialog=builder.create();
			dialog.show();
			}
				
		});
		
		return row;
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)con.getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }

	private static String parseJson(String string)
	{
		String str="";
		JSONObject object = null;
		try
		{
			object=new JSONObject(string);
			str=object.getString("response");
		}
		catch(JSONException exc)
		{
			
		}
		return str;
	}
	
	private static String convertStreamToString(InputStream stream) throws Exception
	{
		StringBuffer buffer=new StringBuffer();
		String line;
		BufferedReader reader=new BufferedReader(new InputStreamReader(stream));
		while((line=reader.readLine()) != null)
		{
			buffer.append(line);
		}
		return buffer.toString();
	}
	
	private static String sendToServer(String string) throws Exception
	{
		InputStream stream;
		HttpClient client=new DefaultHttpClient();
		HttpPost post = new HttpPost(NetworkAddress.deleteuser);
		StringEntity entity=new StringEntity(string);
		post.setEntity(entity);
		
		HttpResponse response=client.execute(post);
		stream=response.getEntity().getContent();
		return convertStreamToString(stream);
	}
	
	private static String createIdString(int id)
	{
		JSONObject json=new JSONObject();
		try
		{
			//json.put("object", "delete");
			json.put("user", id);
		}
		catch(JSONException exc)
		{
			
		}
		return json.toString();
	}
	
	private class DeleteUser extends AsyncTask<Integer,Void,String>
	{
		private String data="",result="",response="";
		
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(con, "deleting", "please wait...");
		}
		
		public String doInBackground(Integer... id)
		{
			
				data=createIdString(id[0]);
				if(accessuser.deleteUser(id[0]))
				{
					try
					{
						result=sendToServer(data);
					}
					catch(Exception exc)
					{
						Log.i("exception","");
					}
				
					response= parseJson(result);
					
					con.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(con, response, Toast.LENGTH_SHORT).show();
						}
					});
					
					Intent i=new Intent(con,ListUsersActivity.class);
					con.startActivity(i);
				}
				else
				{
					con.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(con, "UNABLE TO DELETE USER", Toast.LENGTH_SHORT).show();
						}
					});
					
					Intent intent=new Intent(con,ListUsersActivity.class);
					con.startActivity(intent);
				}
					
			
			return null;
		}
		
		public void onPostExecute(String string)
		{
			
			dialog.dismiss();
			
		}
	}

}