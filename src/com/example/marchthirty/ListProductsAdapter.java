package com.example.marchthirty;

import android.widget.*;
import android.content.*;
import java.util.*;

import android.view.*;

public class ListProductsAdapter extends ArrayAdapter<Product>
{
	private Context con;
	private List<Product> list;
	private TextView idview,nameview,priceview,weightview;
	private ImageView image;
	
	public ListProductsAdapter(Context c,List<Product> l)
	{
		super(c,R.layout.product_grid_items,l);
		this.con=c;
		this.list=l;
	}
	
	public View getView(int pos, View convertView,ViewGroup vg)
	{
		Product theproduct= list.get(pos);
		String name="";
		int id;
		
		LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row=convertView;
		if(row == null)
		{
			row=inflater.inflate(R.layout.product_grid_items, vg,false);
		}
		
		idview=(TextView)row.findViewById(R.id.product_grid_items_productid);
		nameview=(TextView)row.findViewById(R.id.product_grid_items_productname1);
		priceview=(TextView)row.findViewById(R.id.product_grid_items_price);
		weightview=(TextView)row.findViewById(R.id.product_grid_items_weight);
		image=(ImageView)row.findViewById(R.id.product_grid_items_image);
		
		name=theproduct.getProductName();
		id=theproduct.getProductId();
		idview.setText(id+"");
		nameview.setText(name);
		priceview.setText("Price: "+theproduct.getPrice()+"");
		weightview.setText("Weight: "+theproduct.getWeight());
		
		switch(id)
		{
		case 1:
		case 21:
			image.setImageResource(R.drawable.pineapples);
			break;
		case 2:
			image.setImageResource(R.drawable.apples);
			break;
		case 3:
		case 22:
			image.setImageResource(R.drawable.mangoes);
			break;
		case 4:
		case 24:
			image.setImageResource(R.drawable.oranges);
			break;
		case 5:
			image.setImageResource(R.drawable.lemons);
			break;
		case 6:
		case 29:
			image.setImageResource(R.drawable.water_melons);
			break;
		case 7:
			image.setImageResource(R.drawable.guavas);
			break;
		case 8:
			image.setImageResource(R.drawable.pears);
			break;
		case 9:
		case 25:
			image.setImageResource(R.drawable.peaches);
			break;
		case 10:
		case 28:
			image.setImageResource(R.drawable.bananas);
			break;
		case 11:
			image.setImageResource(R.drawable.strawberries);
			break;
		case 12:
		case 30:
			image.setImageResource(R.drawable.blackcurrant);
			break;
		case 13:
			image.setImageResource(R.drawable.passionfruit);
			break;
		case 14:
			image.setImageResource(R.drawable.tomatoes);
			break;
		case 15:
		case 27:
			image.setImageResource(R.drawable.carrots);
			break;
		case 16:
		case 23:
			image.setImageResource(R.drawable.cassava);
			break;
		case 17:
			image.setImageResource(R.drawable.pepper);
			break;
		case 18:
			image.setImageResource(R.drawable.sweet_potatoes);
			break;
		case 19:
		case 26:
			image.setImageResource(R.drawable.pawpaw);
			break;
		case 20:
			image.setImageResource(R.drawable.avocado);
			break;
		}
		
		return row;
	}
}