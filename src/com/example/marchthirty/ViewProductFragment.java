package com.example.marchthirty;

import android.os.*;
import android.view.*;
import android.support.v4.app.Fragment;
import android.widget.*;
import android.content.*;

public class ViewProductFragment extends Fragment
{
	private Product product;
	//private ListView listview;
	//private ViewProductsAdapter adapter;
	private TextView id,name,weight,price;
	private ImageView image;
	
	public ViewProductFragment(Context con,Product pro)
	{
		this.product=pro;
		//adapter=new ViewProductsAdapter(con,product);
	}
	
	public View onCreateView(LayoutInflater inflater,ViewGroup group,Bundle bundle)
	{
		super.onCreateView(inflater, group,bundle);
		
		View view= inflater.inflate(R.layout.view_product_page, group,false);
		
		id=(TextView)view.findViewById(R.id.view_product_page_id);
		name=(TextView)view.findViewById(R.id.view_product_page_name);
		image=(ImageView)view.findViewById(R.id.view_product_page_photo);
		weight=(TextView)view.findViewById(R.id.view_product_page_weight);
		price=(TextView)view.findViewById(R.id.view_product_page_price);
		
		id.setText(""+product.getProductId());
		name.setText("Name: "+product.getProductName());
		
		weight.setText("Weight: "+product.getWeight());
		price.setText("Price: "+product.getPrice());
		
		
		switch(product.getProductId())
		{
		case 1:
		case 21:
			image.setImageResource(R.drawable.pineapples);
			break;
		case 2:
			image.setImageResource(R.drawable.apples);
			break;
		case 3:
		case 22:
			image.setImageResource(R.drawable.mangoes);
			break;
		case 4:
		case 24:
			image.setImageResource(R.drawable.oranges);
			break;
		case 5:
			image.setImageResource(R.drawable.lemons);
			break;
		case 6:
		case 29:
			image.setImageResource(R.drawable.water_melons);
			break;
		case 7:
			image.setImageResource(R.drawable.guavas);
			break;
		case 8:
			image.setImageResource(R.drawable.pears);
			break;
		case 9:
		case 25:
			image.setImageResource(R.drawable.peaches);
			break;
		case 10:
		case 28:
			image.setImageResource(R.drawable.bananas);
			break;
		case 11:
			image.setImageResource(R.drawable.strawberries);
			break;
		case 12:
		case 30:
			image.setImageResource(R.drawable.blackcurrant);
			break;
		case 13:
			image.setImageResource(R.drawable.passionfruit);
			break;
		case 14:
			image.setImageResource(R.drawable.tomatoes);
			break;
		case 15:
		case 27:
			image.setImageResource(R.drawable.carrots);
			break;
		case 16:
		case 23:
			image.setImageResource(R.drawable.cassava);
			break;
		case 17:
			image.setImageResource(R.drawable.pepper);
			break;
		case 18:
			image.setImageResource(R.drawable.sweet_potatoes);
			break;
		case 19:
		case 26:
			image.setImageResource(R.drawable.pawpaw);
			break;
		case 20:
			image.setImageResource(R.drawable.avocado);
			break;
		}
		
		
		return view;
	}
}