package com.example.marchthirty;

import android.view.*;
import android.view.ViewGroup;
import android.widget.*;
import android.content.*;
import java.util.*;

public class ExpandableAdapter extends BaseExpandableListAdapter
{
	private Context context;
	private AccessDatabaseUser accessuser;
	private List<User> users;
	private List<String> options;
	
	public ExpandableAdapter(Context con)
	{
		this.context = con;
		accessuser = new AccessDatabaseUser(context);
		accessuser.open();
		users = accessuser.getAllUsers();
		options = new ArrayList<String>();
		options.add("orders");
		options.add("update");
	}
	
	public int getUserId(int pos)
	{
		return users.get(pos).getId();
	}

	@Override
	public Object getChild(int gid, int cid) {
		
		return options.get(cid);
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		return arg1;
	}

	@Override
	public View getChildView(int gid, int cid, boolean arg2, View view, ViewGroup parent) {
		String child = (String)getChild(gid,cid);
		TextView text = (TextView)view;
		
		if(text == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			text = (TextView)inflater.inflate(R.layout.expandable_listview_child, parent,false);
		}
		
		text.setText(child);
		
		return text;
	}

	@Override
	public int getChildrenCount(int arg0) {
		return options.size();
	}

	@Override
	public Object getGroup(int arg0) {
		return users.get(arg0);
	}

	@Override
	public int getGroupCount() {
		return users.size();
	}

	@Override
	public long getGroupId(int arg0) {
		return users.get(arg0).getId();
	}

	@Override
	public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
		User user = (User)getGroup(arg0);
		
		TextView view = (TextView)arg2;
		
		if(view == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = (TextView)inflater.inflate(R.layout.expandable_listview, arg3,false);
		}
		
		view.setText(user.getFullname());
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}
	
}