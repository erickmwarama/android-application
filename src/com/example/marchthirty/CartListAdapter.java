package com.example.marchthirty;

import android.view.View;
import android.view.*;
import android.widget.*;
import android.content.*;
import java.util.*;

public class CartListAdapter extends BaseExpandableListAdapter
{
	
	private Context context;
	private AccessDatabaseCart accesscart;
	private AccessDatabaseOrder accessorder;
	private AccessDatabaseProducts accessproducts;
	private AccessDatabaseUser accessuser;
	private List<Order> orders;
	private MyCart mycart;
	
	public CartListAdapter(Context con,MyCart cart)
	{
		this.context=con;
		accessproducts = new AccessDatabaseProducts(context);
		accessuser = new AccessDatabaseUser(context);
		accessproducts.open();
		accessuser.open();
		mycart = cart;
		orders = mycart.getOrders();
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return orders.get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return orders.get(childPosition).getOrderId();
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		//accessproducts.open();
		
		Order order = (Order)getChild(groupPosition,childPosition);
		Product product = accessproducts.getProduct(order.getProductId());
		View view = convertView;
		if(view == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.mycartviews, parent,false);
		}
		
		TextView weightview = (TextView)view.findViewById(R.id.mycart_productweight);
		TextView nameview = (TextView)view.findViewById(R.id.mycart_productname);
		TextView priceview = (TextView)view.findViewById(R.id.mycartprice);
		TextView qtyview = (TextView)view.findViewById(R.id.mycartquantity);
		TextView totalview =(TextView)view.findViewById(R.id.mycart_displaytotal);
		
		nameview.setText(product.getProductName());
		weightview.setText("Weight: "+product.getWeight());
		priceview.setText("Price: "+product.getPrice());
		qtyview.setText("Quantity: "+order.getQuantity());
		int total = product.getPrice() * order.getQuantity();
		totalview.setText(total+"");
		
		//accessproducts.close();
		
		return view;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return orders.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		String username = accessuser.getUserName(mycart.getUserId());
		int cartid = mycart.getCartId();
		
		View view = convertView;
		
		if(view == null)
		{
			LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.cartlist_item, parent,false);
		}
		
		TextView idview = (TextView)view.findViewById(R.id.cartlist_id);
		TextView nameview = (TextView)view.findViewById(R.id.cartlist_user);
		
		idview.setText("Cart Id: "+cartid);
		nameview.setText("User: "+username);
		
		return view;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return false;
	}
	
}