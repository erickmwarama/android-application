package com.example.marchthirty;

import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.os.*;
import java.util.*;

public class ProductsByNameActivity extends FragmentActivity
{
	private ViewPager pager;
	private FragmentManager manager;
	//private Map<String,List<Product>> products;
	//private List<String> products;
	private ProductsByNameAdapter adapter;
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.products_viewpager);
		
		//accessproducts = new AccessDatabaseProducts(ProductsByNameActivity.this);
		//products=accessproducts.getAllProductNames();
		pager=(ViewPager)findViewById(R.id.viewpager);
		
		manager=getSupportFragmentManager();
		
		adapter=new ProductsByNameAdapter(ProductsByNameActivity.this,manager);
		pager.setAdapter(adapter);
	}
}