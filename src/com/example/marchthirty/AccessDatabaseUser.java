package com.example.marchthirty;

import android.content.*;
//import android.util.Log;
//import android.widget.*;
import android.database.*;
import android.database.sqlite.*;
import java.util.*;

public class AccessDatabaseUser extends AccessDatabase
{
	private Context con;
	public AccessDatabaseUser(Context c)
	{
		super(c);
		this.con=c;
	}
	
	public int insertUser(User us)
	{
		ContentValues cv=new ContentValues();
		cv.put("FULLNAME", us.getFullname());
		cv.put("EMAIL", us.getEmail());
		cv.put("PASSWORD", us.getPassword());
		
		 return (int)writabledb.insert("USERS", null, cv);
		
		//Log.i("MARCH THIRTY","USER SUCCESSFULLY INSERTED TO DB");
		//Toast.makeText(con, "USER SUCCESFULLY CREATED",Toast.LENGTH_SHORT).show();
	}
	
	public User getUser(String name,String pswd)
	{
		Cursor cur=readabledb.query("USERS", null,"FULLNAME=? AND PASSWORD=?",new String[]{name,pswd},null,null,null);
		if(cur==null || cur.getCount()==0)
		{
			return null;
		}
		else
		{
			return convertCursorToUser(cur);
		}
	}
	
	private User convertCursorToUser(Cursor c)
	{
		User us=new User();
		c.moveToFirst();
		while(!c.isAfterLast())
		{
			us.setId(c.getInt(0));
			us.setFullname(c.getString(1));
			us.setEmail(c.getString(2));
			us.setPassword(c.getString(3));
			c.moveToNext();
			
		}
		
		return us;
	}
	
	public int getUserId(String name,String pswd)
	{
		Cursor cur=readabledb.query("USERS",new String[]{ "_id"}, "FULLNAME=? AND PASSWORD=?", new String[]{name, pswd}, null, null,null);
		return convertCursorToId(cur);
	}
	
	public int convertCursorToId(Cursor cur)
	{
		if(cur!=null)
		{
			cur.moveToFirst();
			return cur.getInt(0);
		}
		else
		{
			return 0;
		}
	}
	
	public List<User> getAllUsers()
	{
		List<User> users=new ArrayList<User>();
		Cursor cur=readabledb.query("USERS", null, null, null, null, null, null, null);
		if(cur==null || cur.getCount()==0)
		{
			return null;
		}
		else
		{
			cur.moveToFirst();
			while(!cur.isAfterLast())
			{
				users.add(convertCursorToUser2(cur));
				
				cur.moveToNext();
			}
			return users;
		}
	}
	
	private User convertCursorToUser2(Cursor cursor)
	{
		User us=new User();
		us.setId(cursor.getInt(0));
		us.setFullname(cursor.getString(1));
		us.setEmail(cursor.getString(2));
		us.setPassword(cursor.getString(3));
		return us;
	}
	
	public String getUserName(int userid)
	{
		Cursor cur=readabledb.query("USERS", null, "_id=?", new String[]{Integer.valueOf(userid).toString()}, null, null, null);
		if(cur!=null && cur.getCount()==1)
		{
			cur.moveToFirst();
			return cur.getString(1);
		}
		else
		{
			return "";
		}
		
	}
	
	public boolean updateUser(User user)
	{
		ContentValues values = new ContentValues();
		values.put("FULLNAME", user.getFullname());
		values.put("EMAIL", user.getEmail());
		values.put("PASSWORD", user.getPassword());
		return writabledb.update("USERS", values, "_id=?", new String[]{Integer.valueOf(user.getId()).toString()}) > 0;
	}
	
	public boolean deleteUser(int userid)
	{
		return writabledb.delete("USERS", "_id=?", new String[]{Integer.valueOf(userid).toString()}) > 0;
	}
	
	public User getUser(int userid)
	{
		Cursor cursor=readabledb.query("USERS", null, "_id=?", new String[]{Integer.valueOf(userid).toString()}, null, null, null, null);
		if(cursor != null)
		{
			cursor.moveToFirst();
			User user=convertCursorToUser2(cursor);
			return user;
		}
		return null;
	}
	
	public int getUserNumber()
	{
		return getAllUsers().size();
	}
}