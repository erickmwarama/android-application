package com.example.marchthirty;

import java.util.List;

import android.widget.*;
import android.support.v4.app.*;
import android.view.*;
import android.content.*;

public class ViewPagerAdapter extends FragmentStatePagerAdapter
{
	private FragmentManager manager;
	private Context context;
	private AccessDatabaseProducts accessproducts;
	
	public ViewPagerAdapter(Context con,FragmentManager fm)
	{
		super(fm);
		this.context=con;
		accessproducts=new AccessDatabaseProducts(context);
		accessproducts.open();
	}

	public int getCount() {

		return accessproducts.getProductCount();
	}

	public Fragment getItem(int pos)
	{
		Product pro=accessproducts.getProduct(pos+1);
		return new ViewProductFragment(context,pro);
	}
	
}