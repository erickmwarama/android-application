package com.example.marchthirty;

import android.view.*;
import android.view.ViewGroup;
import android.widget.*;
import android.content.*;
import java.util.*;

public class ViewProductsAdapter extends BaseAdapter
{
	private Context context;
	private Product product;
	private TextView weight,price;
	
	public ViewProductsAdapter(Context con,Product p)
	{
		super();
		this.context=con;
		this.product=p;
	}

	@Override
	public int getCount() {
		
		return 1;
	}

	@Override
	public Object getItem(int pos) {
		return product;
	}

	@Override
	public long getItemId(int pos) {
		return product.getProductId();
	}

	@Override
	public View getView(int pos, View view, ViewGroup group) {
		Product theproduct=(Product)getItem(pos);
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row=view;
		if(row == null)
		{
			row=inflater.inflate(R.layout.productitems, group,false);
		}
		
		weight=(TextView)row.findViewById(R.id.itemproductweight);
		price=(TextView)row.findViewById(R.id.itemproductprice);
		
		weight.setText("Weight: "+theproduct.getWeight());
		price.setText("Price: "+theproduct.getPrice());
		
		return row;
	}
	
}