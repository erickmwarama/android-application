package com.example.marchthirty;

import android.app.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.widget.*;
import android.util.Log;
import android.view.*;
import android.content.*;

import java.io.*;
//import android.util.*;
import java.util.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;

public class MainActivity extends Activity
{
	private Button login,register;
	private EditText fullname,password;
	private AccessDatabaseUser accessuser;
	private AccessDatabaseProducts accessproducts;
	private MyPreferences userpref;
	private Activity context;
	private User theuser;
	private String p_word,name;
	//private ProgressDialog dialog;
	private int product_count;
	
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.activity_main);
				
		fullname=(EditText)findViewById(R.id.fullname);
		password=(EditText)findViewById(R.id.password);
		login=(Button)findViewById(R.id.buttonlogin);
		register=(Button)findViewById(R.id.buttonregister);
		login.setBackgroundResource(R.drawable.bluebackground);
		register.setBackgroundResource(R.drawable.bluebackground);
		fullname.requestFocus();
		
		accessuser = new AccessDatabaseUser(MainActivity.this);
		accessproducts = new AccessDatabaseProducts(MainActivity.this);
		accessuser.open();
		accessproducts.open();
		
		context = this;
	
		userpref=new MyPreferences(context);
		
		product_count=accessproducts.getProductByNameCount();
		
		if(b != null)
		{
			p_word = b.getString("password");
			name = b.getString("fullname");
			
			fullname.setText(name);
			password.setText(p_word);
		}
		
		login.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//login.setBackgroundResource(R.drawable.bluebackground);
				String name,pass;
				name=fullname.getText().toString();
				pass=password.getText().toString();
				
				
				
				theuser=accessuser.getUser(name, pass);
				if(theuser != null)
				{
					//dialog.show();
					Intent intent=new Intent(context,WelcomeActivity.class);
					//Intent intent = new Intent(context,NavDrawerActivity.class);
					userpref.storeUser(theuser);
					//dialog.dismiss();
					startActivity(intent);
					//v.setBackgroundResource(R.drawable.greenbackground);
				}
				else
				{
					//dialog.dismiss();
					//v.setBackgroundResource(R.drawable.greenbackground);
					Toast.makeText(context, "CLICK ON THE REGISTER BUTTON TO REGISTER", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		register.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//register.setBackgroundResource(R.drawable.bluebackground);
				Intent i=new Intent(context,RegisterFormActivity.class);
				startActivity(i);
				//v.setBackgroundResource(R.drawable.bluebackground);
			}
		});
		
		if(product_count == 0)
		{
			if(checkConnection())
			{
				new GetProducts().execute();
			}
			else
			{
				Toast.makeText(context, "sorry.you do not have internet connection", Toast.LENGTH_SHORT).show();
			}
			
		}
		
	}
	
	public void onResume()
	{
		super.onResume();
		accessuser.open();
		accessproducts.open();
	}
	
	public void onPause()
	{
		super.onPause();
		accessuser.close();
		accessproducts.close();
	}
	
	
	public void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putString("fullname", fullname.getText().toString());
		bundle.putString("password", password.getText().toString());
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }
	
	private static List<Product> extractProducts(String string)
	{
		List<Product> list = new ArrayList<Product>();
		String names="",prices="",weights="",ids="";
		String [] name_array;
		String [] price_array;
		String [] weight_array;
		String [] id_array;
		try
		{
			JSONObject object = new JSONObject(string);
			ids = object.getString("ids");
			prices = object.getString("prices");
			weights = object.getString("weights");
			names = object.getString("names");
			
			ids=ids.substring(1,ids.length()-1);
			prices = prices.substring(1,prices.length()-1);
			weights=weights.substring(1,weights.length()-1);
			names= names.substring(1,names.length()-1);
			
			id_array = ids.split(",");
			price_array = prices.split(",");
			weight_array = weights.split(",");
			name_array = names.split(",");
			
			
			
			for(int i=0;i<id_array.length;i++)
			{
				Product product = new Product();
				product.setProductId(Integer.valueOf(id_array[i]));
				product.setPrice(Integer.valueOf(price_array[i]));
				product.setWeight(weight_array[i].replace('"', ' ').trim());
				product.setProductName(name_array[i].replace('"', ' ').trim());
				
				list.add(product);
			}
		}
		catch(Exception exc)
		{
			Log.e("march thirty1",exc.getMessage());
		}
		
		return list;
	}
	
	private static String convertStreamToString(InputStream stream)
	{
		String line = "";
		StringBuffer buffer = new StringBuffer();
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		try
		{
			while((line=reader.readLine()) != null)
			{
				buffer.append(line);
			}
		}
		catch(IOException exc)
		{
			Log.e("march thirty2",exc.getMessage());
		}
		
		return buffer.toString();
	}
	
	private List<Product> getProductsFromServer() throws Exception
	{
		HttpClient client = new DefaultHttpClient();
		HttpPost post= new HttpPost(NetworkAddress.readproducts);
		HttpResponse response = client.execute(post);
		InputStream stream = response.getEntity().getContent();
		String products= convertStreamToString(stream);
		return extractProducts(products);
	}
	
	private class GetProducts extends AsyncTask<Void,Void,String>
	{
		private ProgressDialog dialog;
		private List<Product> products;
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(context,"downloading products","please wait...");
		}
		
		public String doInBackground(Void...voids)
		{
			
				try
				{
					products = getProductsFromServer();
				}
				catch(Exception exc)
				{
					Log.e("march thirty3",exc.getMessage());
				}
				
				if(accessproducts.insertProducts(products))
				{
					context.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(context, "products inserted in local db", Toast.LENGTH_SHORT).show();
						}
					});
				}
				else
				{
					context.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(context, "error inserting products to local database", Toast.LENGTH_SHORT).show();
						}
					});
				}
						
			
			return null;
		}
		
		public void onPostExecute(String string)
		{
			dialog.dismiss();
		}
	}
	
	
	/*public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(getApplicationContext(),ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(getApplicationContext(),ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(getApplicationContext(),ListProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(getApplicationContext(),MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case R.id.gohome:
			Intent homeIntent = new Intent(getApplicationContext(),MainActivity.class);
			startActivity(homeIntent);
			break;
		 default:
		
		}
	return true;
	}*/
}