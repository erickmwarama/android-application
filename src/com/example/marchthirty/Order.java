package com.example.marchthirty;

public class Order
{

	private int orderid,quantity,productid,userid,cartid;
	private long date;
	
	public int getProductId() {
		return productid;
	}
	
	public int getOrderId()
	{
		return this.orderid;
	}
	
	public void setOrderId(int oid)
	{
		this.orderid=oid;
	}
	
	public int getCartId()
	{
		return this.cartid;
	}
	
	public void setCartId(int cid)
	{
		this.cartid=cid;
	}
	
	public void setProductId(int id) {
		this.productid=id;;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
}