package com.example.marchthirty;

import android.content.*;
import android.database.sqlite.*;
import android.database.*;

import java.util.*;

public class AccessDatabaseCart extends AccessDatabase
{
	private Context context;
	private List<Integer> orders;
	private AccessDatabaseOrder accessorders;
	
	public AccessDatabaseCart(Context cont)
	{
		super(cont);
		this.context=cont;
	}
	
	public void insertCart(MyCart cart)
	{
		if(cartExists(cart))
		{
			return;
		}
		else
		{
			ContentValues values=new ContentValues();
			values.put("USERID", cart.getUserId());
			values.put("STATUS", cart.getStatus());
			writabledb.insert("CART", null, values);
		}
	}
	
	public int getCartId(int userid)
	{
		int cartid;
		Cursor cursor=readabledb.query("CART", new String[]{"_id"}, "USERID=? AND STATUS=?", new String[]{Integer.valueOf(userid).toString(),Integer.valueOf(0).toString()}, null, null, null);
		if(cursor != null && cursor.getCount()==1)
		{
			cursor.moveToFirst();
			cartid=cursor.getInt(0);
		}
		else
		{
			cartid=-1;
		}
		return cartid;
	}
	
	public MyCart getCart(int userid)
	{
		accessorders = new AccessDatabaseOrder(context);
		accessorders.open();
		Cursor cursor = readabledb.query("CART", new String[]{"_id"}, "USERID = ? AND STATUS = ?", new String[]{Integer.valueOf(userid).toString(), Integer.valueOf(0).toString()}, null, null, null);
		if(cursor != null && cursor.getCount() == 1)
		{
			cursor.moveToFirst();
			MyCart cart = new MyCart();
			cart.setCartId(cursor.getInt(0));
			cart.setUserId(userid);
			cart.setStatus(0);
			cart.setOrders(accessorders.getCartOrders(cursor.getInt(0)));
			return cart;
		}
		else
		{
			return null;
		}
	}
	
	public void setCartStatus(int status,MyCart cart)
	{
		ContentValues values = new ContentValues();
		values.put("STATUS", status);
		writabledb.update("CART", values, "_id=?", new String[]{Integer.valueOf(cart.getCartId()).toString()});
	}
	
	public boolean checkCartStatus(int cartid)
	{
		Cursor cursor=readabledb.query("CART", new String[]{"STATUS"}, "_id=?",new String[]{Integer.valueOf(cartid).toString()},null,null,null,null);
		if(cursor !=null && cursor.getCount()==1)
		{
			cursor.moveToFirst();
			if(cursor.getInt(0)==1)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean cartExists(MyCart cart)
	{
		Cursor cursor=readabledb.query("CART", null, "USERID=? AND STATUS=?",new String[]{Integer.valueOf(cart.getUserId()).toString(),Integer.valueOf(cart.getStatus()).toString()},null,null,null,null);
		if(cursor !=null && cursor.getCount()==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}