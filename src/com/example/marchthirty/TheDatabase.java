package com.example.marchthirty;

import android.database.sqlite.*;
//import android.database.*;
import android.content.*;
import android.util.Log;

public class TheDatabase extends SQLiteOpenHelper
{
	//private Context con;
	private static final String dbname="MYAPPDATABASE19";
	private static final int dbversion=1;
	private SQLiteDatabase readabledb,writabledb;
	
	public TheDatabase(Context context)
	{
		super(context,dbname,null,dbversion);
		//this.con=context;
	}
	
	public void onCreate(SQLiteDatabase database)
	{
		String createusers="CREATE TABLE USERS(_id INTEGER PRIMARY KEY AUTOINCREMENT,FULLNAME TEXT,EMAIL TEXT,PASSWORD TEXT)";
		String createorders="CREATE TABLE ORDERS(_id INTEGER PRIMARY KEY AUTOINCREMENT,USERID INTEGER,PRODUCTID INTEGER,QUANTITY INTEGER,DATE REAL,CARTID INTEGER)";
		String createproducts="CREATE TABLE PRODUCTS(_id INTEGER PRIMARY KEY AUTOINCREMENT,PRODUCTNAME TEXT,PRICE INTEGER,WEIGHT TEXT, SERVERID INTEGER)";
		String createcart="CREATE TABLE CART(_id INTEGER PRIMARY KEY AUTOINCREMENT,USERID INTEGER,STATUS INTEGER)";
		database.execSQL(createusers);
		database.execSQL(createorders);
		database.execSQL(createproducts);
		database.execSQL(createcart);
		Log.i("marchthirty","tables have been successfully created");
	}
	
	public void onUpgrade(SQLiteDatabase database, int oldversion, int newversion)
	{
		
	}
	public void onOpen(SQLiteDatabase database)
	{
		Log.i("MARCH THIRTY","database succesfully opened");
	}
}