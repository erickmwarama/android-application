package com.example.marchthirty;

//import android.content.*;
import java.util.*;

public class MyCart
{
	
	private String username;
	private List<Order> orderlist;
	private int cartid,userid,status;
	
	public int getStatus()
	{
		return this.status;
	}
	
	public void setStatus(int st)
	{
		this.status=st;
	}
	
	public int getUserId()
	{
		return this.userid;
	}
	
	public void setUserId(int uid)
	{
		this.userid=uid;
	}
	
	public int getCartId()
	{
		return this.cartid;
	}
	
	public void setCartId(int cid)
	{
		this.cartid=cid;
	}
	
	public void setUserName(String str)
	{
		
		this.username=str;
	}
		
	public void setOrders(List<Order> orders)
	{
		this.orderlist=orders;
	}
	
	public List<Order> getOrders()
	{
		return this.orderlist;
	}
	
	public String getUserName()
	{
		return this.username;
	}
}