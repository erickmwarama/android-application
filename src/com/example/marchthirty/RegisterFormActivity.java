package com.example.marchthirty;

import java.io.*;
import java.util.regex.*;
import java.util.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;

import android.app.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.view.*;
import android.widget.*;
import android.content.*;
//import android.util.*;

public class RegisterFormActivity extends Activity
{
	private EditText newname,newemail,newpassword;
	private TextView message;
	private Button register;
	private AccessDatabaseUser accessuser;
	private ActionBar actionbar;
	private User dummy;
	public Activity context;
	private MyPreferences pref;
	
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.register_form);
		newname=(EditText)findViewById(R.id.newnametext);
		newemail=(EditText)findViewById(R.id.newemailtext);
		newpassword=(EditText)findViewById(R.id.newpasswordtext);
		context=RegisterFormActivity.this;
		//message=(TextView)findViewById(R.id.registermessage);
		register=(Button)findViewById(R.id.buttonregisternew);
		register.setBackgroundResource(R.drawable.bluebackground);
		//login=(Button)findViewById(R.id.registerlogin);
		pref=new MyPreferences(RegisterFormActivity.this);
		
		if(bundle != null)
		{
			newname.setText(bundle.getString("name"));
			newemail.setText(bundle.getString("email"));
			newpassword.setText(bundle.getString("password"));
		}
		
		accessuser = new AccessDatabaseUser(RegisterFormActivity.this);
		accessuser.open();
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		
		register.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//v.setBackgroundResource(R.drawable.bluebackground);
				String n,e,p;
				boolean error=false;
				n=newname.getText().toString();
				e=newemail.getText().toString();
				p=newpassword.getText().toString();
				
				if(n.equals((Object)new String("")))
				{
					error=true;
					Toast.makeText(context, "must provide a full name",Toast.LENGTH_SHORT).show();
				}
				else
				{
					if(e.equals((Object)new String("")))
					{
						error=true;
						Toast.makeText(context, "must provide an email address", Toast.LENGTH_SHORT).show();
					}
					else
					{
						Pattern pat=Pattern.compile("(.+)@(.+).(com)");
						Matcher mat=pat.matcher(e);
						if(!mat.find())
						{
							error=true;
							Toast.makeText(context, "provide a valid email address", Toast.LENGTH_SHORT).show();
						}
						if(p.equals((Object)new String("")))
						{
							error=true;
							Toast.makeText(context, "must create a password", Toast.LENGTH_SHORT).show();
						}
						dummy=accessuser.getUser(n,p);
						if(dummy!=null)
						{
							error=true;
							Toast.makeText(getApplicationContext(), "USER ALREADY EXISTS.CLICK ON LOGIN TO LOGIN", Toast.LENGTH_SHORT).show();
						}
					}
				}
				
				if(!error)
				{
					final User newuser=new User();
					newuser.setEmail(e);
					newuser.setFullname(n);
					newuser.setPassword(p);
					
					if(checkConnection())
					{
						new UploadForm().execute(newuser);
					}
					else
					{
						Toast.makeText(context, "sorry. you do not have internet connection",Toast.LENGTH_SHORT).show();
						
					}
					
					
				}				
			}
		});
		
		
	}
	
	public void onSaveInstanceState(Bundle bundle)
	{
		super.onSaveInstanceState(bundle);
		bundle.putString("name", newname.getText().toString());
		bundle.putString("email", newemail.getText().toString());
		bundle.putString("password", newpassword.getText().toString());
	}
	
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(RegisterFormActivity.this,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(RegisterFormActivity.this,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(RegisterFormActivity.this,ListOfProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(RegisterFormActivity.this,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(RegisterFormActivity.this,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(RegisterFormActivity.this,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(RegisterFormActivity.this,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
	
	private static String parseJson(String string)
	{
		JSONObject object=null;
		String str="";
		try
		{
			object=new JSONObject(string);
			str=object.getString("response");
		}
		catch(Exception exc)
		{
			
		}
		return str;
	}
	
	private String createUserString(User user)
	{
		JSONObject json=new JSONObject();
		try
		{
			
			json.put("userid", user.getId());
			json.put("fullname", user.getFullname());
			json.put("email", user.getEmail());
			json.put("password", user.getPassword());
		}
		catch(JSONException exc)
		{
			
		}
		return json.toString();
	}
	
	private  boolean checkConnection()
    {
    	ConnectivityManager conman=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo netinfo=conman.getActiveNetworkInfo();
    	
    	if(netinfo != null && netinfo.isConnected())
    	{
    		
    		return true;
    	}
    	else {
    		
    		return false;
    		
    	}
    }
	
	private static String convertStreamToString(InputStream stream) throws Exception
	{
		StringBuffer buffer = new StringBuffer();
		String line;
		BufferedReader reader=new BufferedReader(new InputStreamReader(stream));
		while((line=reader.readLine()) != null)
		{
			buffer.append(line);
		}
		return buffer.toString();
	}
	
	private static String sendToServer(String data) throws Exception
	{
		InputStream stream;
		HttpClient client=new DefaultHttpClient();
		HttpPost post= new HttpPost(NetworkAddress.createuser);
		StringEntity entity=new StringEntity(data);
		post.setEntity(entity);
		HttpResponse response=client.execute(post);
		stream=response.getEntity().getContent();
		
		return convertStreamToString(stream);
	}
	
	private class UploadForm extends AsyncTask<User,Void,String>
	{
		private ProgressDialog dialog;
		private int resultid;
		
		public void onPreExecute()
		{
			dialog=ProgressDialog.show(context,"uploading","please wait...");
		}
		
		public String doInBackground(User... users)
		{
			
			String result="",data="";
			
			
				resultid=accessuser.insertUser(users[0]);
				if(resultid != -1)
				{
					users[0].setId(resultid);
					pref.storeUser(users[0]);
					data=createUserString(users[0]);
					try
					{
						result=sendToServer(data);
					}
					catch(Exception exc)
					{
						
					}
					
					final String response=parseJson(result);
					
					context.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
						}
					});
					
					Intent i=new Intent(context,WelcomeActivity.class);
					startActivity(i);
				}
				else
				{
					
					context.runOnUiThread(new Runnable()
					{
						public void run()
						{
							Toast.makeText(context, "UNABLE TO CREATE USER", Toast.LENGTH_SHORT).show();
						}
					});
				}
							
			
			return null;
		}
		
		public void onPostExecute(String s)
		{
			newname.setText("");
			newemail.setText("");
			newpassword.setText("");
			dialog.dismiss();
			
			
		}
	}
}