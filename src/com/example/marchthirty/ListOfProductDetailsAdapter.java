package com.example.marchthirty;

import android.view.*;
import android.widget.*;
import android.content.*;
import java.util.*;

public class ListOfProductDetailsAdapter extends BaseAdapter
{
	private Context context;
	private ImageView image;
	private TextView idview,weightview,priceview;
	private AccessDatabaseProducts accessproducts;
	private int count;
	//private List<Product> list;
	private Product product;
	
	public ListOfProductDetailsAdapter(Context con,int ind)
	{
		super();
		this.context=con;
		accessproducts=new AccessDatabaseProducts(context);
		accessproducts.open();
		this.product=accessproducts.getProduct(ind);
	}

	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int pos) {
		return product;
	}

	@Override
	public long getItemId(int pos) {
		return product.getProductId();
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		Product product = (Product)getItem(arg0);
		
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row=arg1;
		if(row == null)
		{
			row=inflater.inflate(R.layout.products_by_name_list, arg2,false);
		}
		idview=(TextView)row.findViewById(R.id.products_by_name_list_id);
		weightview=(TextView)row.findViewById(R.id.products_by_name_list_weight);
		priceview=(TextView)row.findViewById(R.id.products_by_name_list_price);
		image=(ImageView)row.findViewById(R.id.products_by_name_list_image);
		
		idview.setText("Id: "+product.getProductId());
		weightview.setText("Weight: "+product.getWeight());
		priceview.setText("Price: "+product.getPrice());
		
		switch(product.getProductId())
		{
		case 1:
		case 21:
			image.setImageResource(R.drawable.pineapples);
			break;
		case 2:
			image.setImageResource(R.drawable.apples);
			break;
		case 3:
		case 22:
			image.setImageResource(R.drawable.mangoes);
			break;
		case 4:
		case 24:
			image.setImageResource(R.drawable.oranges);
			break;
		case 5:
			image.setImageResource(R.drawable.lemons);
			break;
		case 6:
		case 29:
			image.setImageResource(R.drawable.water_melons);
			break;
		case 7:
			image.setImageResource(R.drawable.guavas);
			break;
		case 8:
			image.setImageResource(R.drawable.pears);
			break;
		case 9:
		case 25:
			image.setImageResource(R.drawable.peaches);
			break;
		case 10:
		case 28:
			image.setImageResource(R.drawable.bananas);
			break;
		case 11:
			image.setImageResource(R.drawable.strawberries);
			break;
		case 12:
		case 30:
			image.setImageResource(R.drawable.blackcurrant);
			break;
		case 13:
			image.setImageResource(R.drawable.passionfruit);
			break;
		case 14:
			image.setImageResource(R.drawable.tomatoes);
			break;
		case 15:
		case 27:
			image.setImageResource(R.drawable.carrots);
			break;
		case 16:
		case 23:
			image.setImageResource(R.drawable.cassava);
			break;
		case 17:
			image.setImageResource(R.drawable.pepper);
			break;
		case 18:
			image.setImageResource(R.drawable.sweet_potatoes);
			break;
		case 19:
		case 26:
			image.setImageResource(R.drawable.pawpaw);
			break;
		case 20:
			image.setImageResource(R.drawable.avocado);
			break;
		default:
			break;
				
		}
		
		return row;
	}
}