package com.example.marchthirty;

import android.app.*;
import android.os.*;
import android.widget.*;
import android.view.*;
import android.content.*;

public class WelcomeActivity extends Activity
{
	private Button order;
	private MyPreferences mypref;
	private TextView hello;
	private ActionBar actionbar;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.welcome);
		
		order=(Button)findViewById(R.id.welcomeorder);
		order.setBackgroundResource(R.drawable.bluebackground);
		hello=(TextView)findViewById(R.id.helloview);
		
		mypref=new MyPreferences(getApplicationContext());
		User usr=mypref.retreiveUser();
		String pro="HELLO "+usr.getFullname();
		hello.setText(pro);
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		order.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Intent i=new Intent(getApplicationContext(),CreateOrderActivity.class);
				startActivity(i);
			}
		});
		
	}
	
		
	
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(WelcomeActivity.this,NavDrawerActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(WelcomeActivity.this,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(WelcomeActivity.this,ListOfProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(WelcomeActivity.this,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(WelcomeActivity.this,MainActivity.class);
			homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(WelcomeActivity.this,ViewProductsActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(WelcomeActivity.this,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
	
}