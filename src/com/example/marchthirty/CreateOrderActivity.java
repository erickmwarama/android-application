package com.example.marchthirty;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.widget.*;
import android.view.*;
import android.content.*;
import java.util.*;

//import java.util.Date;

public class CreateOrderActivity extends Activity
{
	
	private Context con;
	private EditText txt_quantity;
	private AutoCompleteTextView productnameview;
	private ListView productlist;
	private MyPreferences mypref;
	private AccessDatabaseProducts accessproducts;
	public TextView nameview,weightview,priceview;
	private AccessDatabaseCart accesscart;
	private AccessDatabaseOrder accessorder;
	private AccessDatabaseUser accessuser;
	private ProductNameAdapter nameadapter;
	private List<String> productnames;
	private ActionBar actionbar;
	private Button btn_addtocart,btn_viewcart;
	public MyCart cart;
	private String productname,quantity;
	private int productid;
	private List<Product> allproducts;
	public static String p_name,p_weight,tprice;
	public static int p_price;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.createorder);
		
		productnameview=(AutoCompleteTextView)findViewById(R.id.createorder_prod_name);
		btn_addtocart=(Button)findViewById(R.id.createorder_addtocart);
		btn_viewcart=(Button)findViewById(R.id.createorder_viewcart);
		txt_quantity=(EditText)findViewById(R.id.createorder_qtyview);
		productnameview.setFocusable(true);
		productnameview.setFocusableInTouchMode(true);
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		con=CreateOrderActivity.this;
		mypref=new MyPreferences(con);
		
		accessproducts=new AccessDatabaseProducts(con);
		accesscart=new AccessDatabaseCart(con);
		accessuser = new AccessDatabaseUser(con);
		accessorder=new AccessDatabaseOrder(con);
		
		accesscart.open();
		accessuser.open();
		accessproducts.open();
		accessorder.open();
		
		cart=new MyCart();
		cart.setUserId(accessuser.getUserId(mypref.retreiveUser().getFullname(), mypref.retreiveUser().getPassword()));
		cart.setStatus(0);
		
		productnames=accessproducts.getAllProductNames();
		allproducts=accessproducts.getAllProducts();
		
		nameadapter=new ProductNameAdapter(con,null);
		productnameview.setAdapter(nameadapter);
		productnameview.setThreshold(2);
		
		productnameview.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			public void onItemClick(AdapterView<?>adapter,View v,int pos,long l)
			{
				nameview=(TextView)v.findViewById(R.id.order_list_name);
				weightview=(TextView)v.findViewById(R.id.order_list_weight);
				priceview=(TextView)v.findViewById(R.id.order_list_price);
				
				p_name= (String)nameview.getText();
				p_weight=weightview.getText().toString();
				tprice=priceview.getText().toString();
				p_weight=p_weight.substring("weight: ".length());
				tprice=tprice.substring("price: ".length());
				p_weight=p_weight.trim();
				tprice=tprice.trim();
				p_price=Integer.valueOf(tprice);
			}
		});
		
		btn_addtocart.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				accesscart.insertCart(cart);
				if(productnames.contains(productnameview.getText().toString()))
				{
					if(!txt_quantity.getText().toString().equals(""))
					{
						Order order = new Order();
						order.setCartId(accesscart.getCartId(cart.getUserId()));
						order.setProductId(accessproducts.getProductId(p_name,p_weight,p_price));
						order.setQuantity(Integer.valueOf(txt_quantity.getText().toString()));
						order.setUserid(cart.getUserId());
						order.setDate((new Date()).getTime());
						
						accessorder.insertOrder(order);
						Toast.makeText(con, "ORDER INSERTED INTO CART.YOU CAN ADD ANOTHER ORDER", Toast.LENGTH_SHORT).show();
						productnameview.setText("");
						txt_quantity.setText("");
						productnameview.requestFocus();
					}
					else
					{
						Toast.makeText(con, "PLEASE PROVIDE A QUANTITY", Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(con, "PLEASE ENTER A VALID PRODUCT", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		btn_viewcart.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				accesscart.close();
				accessuser.close();
				accessproducts.close();
				accessorder.close();
				Intent viewcart=new Intent(con,MyCartActivity.class);
				startActivity(viewcart);
				
			}
		});
	}
	
	public void onResume()
	{
		super.onResume();
		accesscart.open();
		accessuser.open();
		accessproducts.open();
		accessorder.open();
		
		cart=new MyCart();
		cart.setUserId(accessuser.getUserId(mypref.retreiveUser().getFullname(), mypref.retreiveUser().getPassword()));
		cart.setStatus(0);
		
		productnames=accessproducts.getAllProductNames();
		allproducts=accessproducts.getAllProducts();
	}
	
	public void onPause()
	{
		super.onPause();
		accessproducts.close();
		accesscart.close();
		accessuser.close();
		accessorder.close();
		productnames = null;
		allproducts = null;
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(con,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(con,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(con,ListOfProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(con,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(con,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(con,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(con,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
	
	
}