package com.example.marchthirty;

public class User
{
	private String fullname,email,password;
	private int id;
	
	public void setId(int idn)
	{
		this.id=idn;
	}
	
	public int getId()
	{
		return this.id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}