package com.example.marchthirty;

import android.support.v4.app.*;
import android.content.*;

public class ListOfProductsPagerAdapter extends FragmentStatePagerAdapter
{
	private Context context;
	private AccessDatabaseProducts accessproducts;
	
	public ListOfProductsPagerAdapter(Context con,FragmentManager fm)
	{
		super(fm);
		this.context=con;
		accessproducts= new AccessDatabaseProducts(context);
		accessproducts.open();
	}

	@Override
	public Fragment getItem(int pos) {
		return new ListOfProductDetailsFragment(pos+1);
	}

	@Override
	public int getCount() {
		return accessproducts.getProductByNameCount();
	}
}