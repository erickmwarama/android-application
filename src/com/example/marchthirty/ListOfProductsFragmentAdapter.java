package com.example.marchthirty;

import android.content.*;
import android.view.*;
import android.view.ViewGroup;
import android.widget.*;

public class ListOfProductsFragmentAdapter extends BaseAdapter
{
	private AccessDatabaseProducts accessproducts;
	private Context con;
	
	public ListOfProductsFragmentAdapter(Context context)
	{
		super();
		this.con=context;
		accessproducts=new AccessDatabaseProducts(context);
		accessproducts.open();
	}


	@Override
	public int getCount() {
		return accessproducts.getProductByNameCount();
	}


	@Override
	public Object getItem(int pos) {
		return accessproducts.getProduct(pos+1);
	}


	@Override
	public long getItemId(int arg0) {
		return arg0;
	}


	@Override
	public View getView(int pos, View view, ViewGroup group) {
		LayoutInflater inflater =(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView item=(TextView)view;
		Product product=(Product)getItem(pos);
		
		if(item == null)
		{
			item=(TextView)inflater.inflate(R.layout.textview, group,false);
		}
		
		item.setText(product.getProductName());
		
		return item;
	}
}