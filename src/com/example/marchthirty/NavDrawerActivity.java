package com.example.marchthirty;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.*;
import android.view.*;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.widget.*;

import java.util.*;

public class NavDrawerActivity extends Activity
{
	public static DrawerLayout drawerlayout;
	public static ExpandableListView drawerlist;
	private ExpandableAdapter adapter;
	private ActionBarDrawerToggle actiontoggle;
	private ClickListener listener;
	private ChildClickListener childlistener;
	
	
	@SuppressWarnings("deprecation")
	public void onCreate(Bundle bundle)
	{
		super.onCreate(bundle);
		setContentView(R.layout.nav_drawer);
		
		drawerlayout=(DrawerLayout)findViewById(R.id.drawer);
		
		listener = new ClickListener(this);
		childlistener = new ChildClickListener(this);
		
		drawerlist = (ExpandableListView)findViewById(R.id.slidermenu);
				
		adapter= new ExpandableAdapter(NavDrawerActivity.this);
		
		drawerlist.setAdapter(adapter);
		
		//drawerlist.setOnItemClickListener(listener);
		drawerlist.setOnChildClickListener(childlistener);
				
		actiontoggle = new ActionBarDrawerToggle(this,drawerlayout,R.drawable.toggle,R.string.app_name,R.string.app_name)
		{
			public void onDrawerOpened(View v)
			{
				invalidateOptionsMenu();
			}
			
			public void onDrawerClosed(View v)
			{
				invalidateOptionsMenu();
			}
		};
		
		drawerlayout.setDrawerListener(actiontoggle);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setDisplayUseLogoEnabled(true);
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(actiontoggle.onOptionsItemSelected(item))
		{
			
			return true;
		}
		
		
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent userintent = new Intent(NavDrawerActivity.this,ListUsersActivity.class);
			startActivity(userintent);
			break;
		case R.id.listorders:
			Intent orderintent = new Intent(NavDrawerActivity.this,ListOrdersActivity.class);
			startActivity(orderintent);
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		boolean isvisible = drawerlayout.isDrawerOpen(drawerlist);
		menu.findItem(R.id.viewmycart).setVisible(!isvisible);
		menu.findItem(R.id.listproducts).setVisible(!isvisible);
		menu.findItem(R.id.productsbyname).setVisible(!isvisible);
		menu.findItem(R.id.pager).setVisible(!isvisible);
		return super.onPrepareOptionsMenu(menu);
	}
	
	public void onPostCreate(Bundle bundle)
	{
		super.onPostCreate(bundle);
		actiontoggle.syncState();
	}
	
	public void onConfigurationChanged(Configuration config)
	{
		super.onConfigurationChanged(config);
		actiontoggle.onConfigurationChanged(config);
	}
}

class ClickListener implements AdapterView.OnItemClickListener
{
	private FragmentManager manager;
	private Activity activity;
	public ClickListener(Context con)
	{
		activity = (Activity)con;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View arg1, int arg2, long arg3) {
		int userid = (int)parent.getAdapter().getItemId(arg2);
		manager = activity.getFragmentManager();
		FragmentTransaction transaction = manager.beginTransaction();
		transaction.replace(R.id.nav_drawer_fragment, new UserOrdersFragment(userid));
		transaction.commit();
		NavDrawerActivity.drawerlayout.closeDrawers();
	}
}


	class ChildClickListener implements ExpandableListView.OnChildClickListener
	{
		private Activity activity;
		
		public ChildClickListener(Context con)
		{
			activity = (Activity)con;
		}

		@Override
		public boolean onChildClick(ExpandableListView parent, View arg1,
				int arg2, int arg3, long arg4) {
			ExpandableAdapter adapter = (ExpandableAdapter)parent.getExpandableListAdapter();
			int userid =(int)adapter.getGroupId(arg2);
			TextView view = (TextView)arg1;
			FragmentManager manager = activity.getFragmentManager();
			FragmentTransaction transaction = manager.beginTransaction();
			if(view.getText().toString().equals("orders"))
			{
				//transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				transaction.replace(R.id.nav_drawer_fragment,new UserOrdersFragment(userid));
				transaction.addToBackStack("name");
				transaction.commit();
			}
			else
			{
				transaction.replace(R.id.nav_drawer_fragment, new UpdateUserFragment(userid));
				transaction.addToBackStack(null);
				transaction.commit();
			}
			
			NavDrawerActivity.drawerlayout.closeDrawers();
			NavDrawerActivity.drawerlist.collapseGroup(arg2);
			return true;
		}
		
	
	
}