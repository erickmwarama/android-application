package com.example.marchthirty;

import android.support.v4.app.*;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.content.*;
import java.util.*;

public class ProductsByNameAdapter extends FragmentStatePagerAdapter
{
	//private List<String> products;
	private AccessDatabaseProducts accessproducts;
	private Context context;
	
	public ProductsByNameAdapter(Context con,FragmentManager man)
	{
		super(man);
		//this.products=names;
		this.context=con;
		accessproducts=new AccessDatabaseProducts(context);
		accessproducts.open();
	}

	@Override
	public Fragment getItem(int pos) {
		String name=accessproducts.getProductName(pos+1);
		return new ProductsByNameFragment(context,name);
	}

	@Override
	public int getCount() {
		
		return accessproducts.getProductByNameCount();
	}
}