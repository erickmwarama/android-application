package com.example.marchthirty;

import android.widget.*;
import android.content.*;
import java.util.*;
import android.view.*;
import java.text.*;

public class ListOrdersAdapter extends BaseAdapter
{
	private Context con;
	private List<Order> thelist;
	private TextView idview,productview,cartview,userview,qtyview,dateview;
	private AccessDatabaseUser accessuser;
	private AccessDatabaseProducts accessproducts;
	
	
	
	public ListOrdersAdapter(Context c,List<Order> list)
	{
		super();
		this.con=c;
		this.thelist=list;
		accessuser = new AccessDatabaseUser(con);
		accessproducts = new AccessDatabaseProducts(con);
		accessuser.open();
		accessproducts.open();
	}
		
	private String convertUserIdToString(int i)
	{
		return accessuser.getUserName(i);
	}
	
	private String displayDate(Date d)
	{
		SimpleDateFormat format=new SimpleDateFormat();
		return format.format(d);
	}
	
	private String createDate(long timestamp)
	{
		Date date=new Date(timestamp);
		return displayDate(date);
	}
	
	public View getView(int pos,View convertView, ViewGroup vg)
	{
		LayoutInflater inflater=(LayoutInflater)con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		Order order=(Order)getItem(pos);
		
		View row=convertView;
		if(row == null)
		{
			row=inflater.inflate(R.layout.orderlist,vg,false);
		}
		
		idview=(TextView)row.findViewById(R.id.listorderid);
		userview=(TextView)row.findViewById(R.id.listorderuser);
		productview=(TextView)row.findViewById(R.id.listorderproductname);
		cartview=(TextView)row.findViewById(R.id.listordercart);
		qtyview=(TextView)row.findViewById(R.id.listorderquantity);
		dateview=(TextView)row.findViewById(R.id.listorderdate);
		
		if(getItemId(pos) % 2==0)
		{
			row.setPadding(10,0,0,0);
		}
		else
		{
			row.setPadding(0, 0, 10, 0);
		}
		
		idview.setText(order.getOrderId()+"");
		userview.setText("User: "+convertUserIdToString(order.getUserid()));
		productview.setText(accessproducts.getProductName(order.getProductId()));
		cartview.setText("Cart id: "+order.getCartId());
		qtyview.setText("Quantity: "+order.getQuantity());
		dateview.setText("Date: "+createDate(order.getDate()));
		
		return row;
	}

	@Override
	public int getCount() {
		
		return this.thelist.size();
	}

	@Override
	public Object getItem(int position) {
		
		return thelist.get(position);
	}

	@Override
	public long getItemId(int position) {
		
		return thelist.get(position).getOrderId();
	}
}