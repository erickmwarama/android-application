package com.example.marchthirty;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.*;

public class ListOrdersActivity extends Activity
{
	private ListView orderslist;
	private ListOrdersAdapter orderadapter;
	private ActionBar actionbar;
	private AccessDatabaseOrder accessorder;	
	private List<Order> orders;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		setContentView(R.layout.list_orders_activity);
		orderslist=(ListView)findViewById(R.id.listorders);
		accessorder = new AccessDatabaseOrder(getApplicationContext());
		accessorder.open();
		orders=accessorder.getAllOrders();
		orderadapter=new ListOrdersAdapter(getApplicationContext(),orders);
		orderslist.setAdapter(orderadapter);
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		orderslist.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			public void onItemClick(AdapterView<?> adapter,View v,int pos,long l)
			{
				Order order=(Order)orderadapter.getItem(pos);
				//Toast.makeText(ListOrdersActivity.this, "orderid = "+order.getOrderId(),Toast.LENGTH_LONG).show();
				Intent intent=new Intent(ListOrdersActivity.this,ViewOrderActivity.class);
				intent.putExtra("orderid",(int) order.getOrderId());
				intent.putExtra("productid",order.getProductId());
				intent.putExtra("cartid", order.getCartId());
				intent.putExtra("quantity", order.getQuantity());
				intent.putExtra("date", order.getDate());
				intent.putExtra("userid", order.getUserid());
				accessorder.close();
				startActivity(intent);
			}

		});
	}

	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(ListOrdersActivity.this,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(ListOrdersActivity.this,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(ListOrdersActivity.this,ListProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(ListOrdersActivity.this,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(ListOrdersActivity.this,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(ListOrdersActivity.this,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(ListOrdersActivity.this,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
}