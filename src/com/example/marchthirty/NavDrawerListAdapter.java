package com.example.marchthirty;

import android.view.*;
import android.view.ViewGroup;
import android.widget.*;
import android.content.*;
import android.app.*;
import java.util.*;

public class NavDrawerListAdapter extends BaseAdapter
{
	private Activity activity;
	private List<User> list;
	private AccessDatabaseUser accessuser;
	
	public NavDrawerListAdapter(Context context)
	{
		activity = (Activity)context;
		accessuser = new AccessDatabaseUser(activity);
		accessuser.open();
		list = accessuser.getAllUsers();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return list.get(pos);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0).getId();
	}

	@Override
	public View getView(int pos, View v, ViewGroup group) {
		User user =(User)getItem(pos);
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		TextView view =(TextView) v;
		if(view == null)
		{
			view=(TextView)inflater.inflate(R.layout.fragment_textview, group,false);
		}
		
		view.setText(user.getFullname());
		
		return view;
	}
	
}