package com.example.marchthirty;

import android.app.*;
import android.content.Intent;
import android.os.*;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import java.util.*;
//import android.util.*;

public class ListUsersActivity extends Activity
{
	private ListView usersview;
	private ListUsersAdapter adapter;
	private List<User> users;
	private AccessDatabaseUser accessuser;
	private ActionBar actionbar;
	
	public void onCreate(Bundle b)
	{
		super.onCreate(b);
		
		setContentView(R.layout.list_users_activity);
		
		//Log.i("MARCH THIRTY","users returned");
		usersview=(ListView)findViewById(R.id.usersview);
		
		actionbar=getActionBar();
		actionbar.setDisplayHomeAsUpEnabled(true);
		actionbar.setDisplayUseLogoEnabled(true);
		
		accessuser=new AccessDatabaseUser(ListUsersActivity.this);
		
	}
	
	public void onStart()
	{
		super.onStart();
		accessuser.open();
		users=accessuser.getAllUsers();
		
		if(!users.isEmpty())
		{
			adapter=new ListUsersAdapter(this,users);
			usersview.setAdapter(adapter);
		}
		else
		{
			Toast.makeText(ListUsersActivity.this, "NO USERS IN SYSTEM", Toast.LENGTH_SHORT).show();
		}
	}
	
	public void onStop()
	{
		super.onStop();
		accessuser.close();
		users= null;
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.mymenu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		switch(item.getItemId())
		{
		case R.id.listusers:
			Intent intent=new Intent(ListUsersActivity.this,ListUsersActivity.class);
			startActivity(intent);
			break;
		case R.id.listorders:
			Intent orderIntent=new Intent(ListUsersActivity.this,ListOrdersActivity.class);
			startActivity(orderIntent);
			break;
		case R.id.listproducts:
			Intent productIntent = new Intent(ListUsersActivity.this,ListOfProductsActivity.class);
			startActivity(productIntent);
			break;
		case R.id.viewmycart:
			Intent cartIntent = new Intent(ListUsersActivity.this,MyCartActivity.class);
			startActivity(cartIntent);
			break;
		case android.R.id.home:
			Intent homeIntent = new Intent(ListUsersActivity.this,MainActivity.class);
			startActivity(homeIntent);
			break;
		case R.id.pager:
			Intent pagerIntent = new Intent(ListUsersActivity.this,WelcomeActivity.class);
			startActivity(pagerIntent);
			break;
		case R.id.productsbyname:
			Intent bynameIntent = new Intent(ListUsersActivity.this,ProductsByNameActivity.class);
			startActivity(bynameIntent);
			break;
		 default:
		
		}
	return true;
	}
}